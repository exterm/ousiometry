"""Setup """

from setuptools import setup


def get_requirements():
    """Get list of requirements."""
    with open('requirements.txt') as f:
        requirements = f.read().splitlines()

    return requirements


PACKAGE_NAME = 'ousiometry'

setup(
    name=PACKAGE_NAME,
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description='Tools for Ousiometry',
    author='UVM Computational Storylab',
    python_requires='>=3.8',
    install_requires=get_requirements(),
)