function A = row(B)
% A = row(B)
% reshape a matrix into one row
A = reshape(B,1,prod(size(B)));
