%% load average scores

%% change path if needed
load('~/matlab/ousiometry/figures/data/VAD_GES_PDSdata.mat');

%% load definitions/notations for ousiometer and ousiocartographer

load_ousiometry_settings;

more off;

%%% colors

loadcolors;

%%% 


figure('visible','off');
set(gcf,'color','none');


tmpfigh = gcf;


clf;
figshape(1000,1000);
%% automatically create postscript whenever
%% figure is drawn
tmpfilename = 'figmeaning_svd_clean250';

tmpfilenoname = sprintf('%s_noname',tmpfilename);
tmpfilesharename = sprintf('%s_',tmpfilename);

%% global switches

set(gcf,'Color','none');
set(gcf,'InvertHardCopy', 'off');

set(gcf,'DefaultAxesFontname','times');
set(gcf,'Renderer','Painters');

set(gcf,'DefaultAxesColor','none');
set(gcf,'DefaultLineMarkerSize',10);
% set(gcf,'DefaultLineMarkerEdgeColor','k');
set(gcf,'DefaultLineMarkerFaceColor','w');
set(gcf,'DefaultAxesLineWidth',0.5);

set(gcf,'PaperPositionMode','auto');

%% tmpsym = {'ok-','sk-','dk-','vk-','^k-','>k-','<k-','pk-','hk-'};
%% tmpsym = {'k-','r-','b-','m-','c-','g-','y-'};
%% tmpsym = {'k-','k-.','k:','k--','r-','r-.','r:','r--'};
%% tmplw = [ 1.5*ones(1,4), .5*ones(1,4)];


%% one simple plot
axes_positions(1).box = [.20 .20 .70 .70];

%% %% create an array of plots
%% 
%% xoffset = 0.05;
%% yoffset = 0.05; 
%% width = .30;
%% height = .30; 
%% xsep = 0.02;
%% ysep = 0.02;
%% numrows = 1; 
%% numcols = 3;
%% axes_positions = makeaxes_positions_grid(xoffset,...
%%                             yoffset,...
%%                             width,...
%%                             height,...
%%                             xsep,...
%%                             ysep,...
%%                             numrows,...
%%                             numcols);

%% %% create the same inset axis for each axis
%% 
%% 
%%  relative_xoffset = 0.10;
%%  relative_yoffset = 0.70;
%%  relative_width = 0.20;
%%  relative_height = 0.20;
%% 
%% inset_positions = makeaxes_inset_positions(...
%%    axes_positions,...
%%    relative_xoffset,...
%%    relative_yoffset,...
%%    relative_width,...
%%    relative_height)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% max counts (not great)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

binsize = 0.1/3;

%% 1-2

binsize = 0.1/3;
xbinedges = [-1:binsize:1]';
ybinedges = [-1:binsize:1]';
edges{1} = xbinedges;
edges{2} = ybinedges;

[counts,centers] = hist3(avgs2d(1:2,:)',...
                         'edges',edges);

tmpmaxcounts(1) = max(counts(:));

%% 1-3

binsize = 0.1/3;
xbinedges = [-1:binsize:1]';
ybinedges = [-1:binsize:1]';
edges{1} = xbinedges;
edges{2} = ybinedges;

[counts,centers] = hist3(avgs2d([1 3],:)',...
                         'edges',edges);

tmpmaxcounts(2) = max(counts(:));

%% 2-3

binsize = 0.1/3;
xbinedges = [-1:binsize:1]';
ybinedges = [-1:binsize:1]';
edges{1} = xbinedges;
edges{2} = ybinedges;

[counts,centers] = hist3(avgs2d([2 3],:)',...
                         'edges',edges);

tmpmaxcounts(3) = max(counts(:));

maxcounts = max(tmpmaxcounts);
tmpmaxcounts


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% histogram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

binsize = 0.1/3;
xbinedges = [-1:binsize:1]';
ybinedges = [-1:binsize:1]';
edges{1} = xbinedges;
edges{2} = ybinedges;

[counts,centers] = hist3(avgs2d(1:2,:)',...
                         'edges',edges);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% main plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

axesnum = 1;
tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);




%% colors

loadcolors;
heatmapcolors = flipud(magma(overallmaxcounts));

for i=1:size(counts,1)-1
    for j=1:size(counts,2)-1
        if (counts(i,j) > 0)
            tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
            tmph = rectangle('position',tmppos);
            hold on;
            
            colorindex = counts(i,j);
            set(tmph,'facecolor',heatmapcolors(colorindex,:));
            set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
 
            
        end
    end
end


%% tmph = plot(avgs2d(1,:),avgs2d(2,:),'.');
%% set(tmph,'markersize',4);
%% set(tmph,'color',colors.verydarkgrey);


set(gca,'color',colors.lightergrey);
set(gca,'fontsize',18)

%% axis([]);
% xlim([-.3 1.4]);
% ylim([-.2 1.5]);

%% xlim([-.85 .85]);
%% ylim([-.85 .85]);

xlim([-1 1]);
ylim([-1 1]);

grid on;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmpcolorbarpos = [-.975 +0.65];

addcolorbar;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% marginals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

offset = 0.0;

maxmarginalcounts = 300;

set(gca,'clipping','off');

for i=1:size(xcounts,1)-1
    if (xcounts(i) > 0)
        tmppos = [xbinedges(i), 1+offset, binsize, binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = 1+round(maxmarginalcounts*xcounts(i)/max(xcounts));
        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
    end
end

for i=1:size(ycounts,2)-1
    if (ycounts(i) > 0)
        tmppos = [1+offset, ybinedges(i), binsize, binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        colorindex = 1+round(maxmarginalcounts*ycounts(i)/max(ycounts));
        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

annotatedwords = zeros(length(words),1);

tmpoffset = 0.02;

annotation_settings.binwidth = 1/20;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: convex hull
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

annotation_settings.binwidth = 1/20;
annotation_settings.fontsize = 20;
annotation_settings.breakangle = 3/4 * pi;
annotation_settings.textoffset = 0.05;
%% set in loadVAD:
%% annotation_settings.excludedtypes = {''}
%% {'peaceful','peacetime','blessing','idle'};

annotate_histogram_convexhull_types001( ...
    avgs2d(1,:)', ...
    avgs2d(2,:)', ...
    words, ...
    annotation_settings);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: medians along main column
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

idim = 2;
otherdims = [1 3];
N = floor((max(avgs2d(idim,:)) - min(avgs2d(idim,:)))/annotation_settings.binwidth);

binedges = linspace(min(avgs2d(idim,:)),...
                    max(avgs2d(idim,:)),...
                    N + 1);

for i=1:N
    indices = find(((avgs2d(idim,:))>=binedges(i)) & ...
                   ((avgs2d(idim,:))<binedges(i+1)));
    [~,index] = min(sum(avgs2d(otherdims,indices).^2));
    i
    if (length(index)>0)
        indices(index)
        word = words{indices(index)}
        
        tmpXcoord = 0;
        tmpYcoord = binedges(i) + annotation_settings.binwidth/2;
        tmpstr = sprintf('%s',word);

        if (mod(i,2)==1)
            tmpcolor = colors.darkergrey;
        else
            tmpcolor = colors.verydarkgrey;
        end

        tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                    'fontsize',18,...
                    'units','data',...
                    'horizontalalignment','center',...
                    'color',tmpcolor, ...
                    'verticalalignment','middle',...
                    'interpreter','latex');

    end    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: rows
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

idim = 1;
otherdims = [2 3];
N = floor((max(avgs2d(idim,:)) - min(avgs2d(idim,:)))/annotation_settings.binwidth);

binedges = linspace(min(avgs2d(idim,:)),...
                    max(avgs2d(idim,:)),...
                    N + 1);

offsets = [0]; %% [-.56 -.28 .28 .56];
for j=1:length(offsets)
    offset = offsets(j);
    for i=[2:10, 15:N-1]
        if (i<=10)
            angle = 90;
        else
            angle = 90;
        end
        indices = find(((avgs2d(idim,:))>=binedges(i)) & ...
                       ((avgs2d(idim,:))<binedges(i+1)));
        [~,index] = min(sum((avgs2d(otherdims,indices)-[offset 0]').^2));
        i
        if (length(index)>0)
            indices(index)
            word = words{indices(index)}
            
            tmpXcoord = binedges(i) + annotation_settings.binwidth/2;
            tmpYcoord = offset;
            tmpstr = sprintf('%s',word);

            if (mod(i+j,2)==1)
                tmpcolor = colors.darkergrey;
            else
                tmpcolor = colors.verydarkgrey;
            end

            tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                        'fontsize',18,...
                        'units','data',...
                        'horizontalalignment','center',...
                        'color',tmpcolor, ...
                        'rotation',angle,...
                        'verticalalignment','middle',...
                        'interpreter','latex');

        end    
    end    
end        

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: off diagonal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


N = 12;

angles = (90*[0:3] + 45)*pi/180;
wordangles = [-45 45 -45 45];
labelindices = [5 0; 4 0; 4 -3; 4 -1];
for j=1:length(angles)
    medianvals = [ cos(angles(j))*annotation_settings.binwidth * (1:N);
                   sin(angles(j))*annotation_settings.binwidth * (1:N);
                   0*(1:N)];

    for i=labelindices(j,1):N+labelindices(j,2)
        [~,indices] = sort(sum((avgsPDS - medianvals(:,i)).^2,1),'ascend');
        index = indices(1);

        if (length(index)>0)
            word = words{index};
            
            tmpXcoord = medianvals(1,i);
            tmpYcoord = medianvals(2,i);
            tmpstr = sprintf('%s',word);

            if (mod(i+j,2)==1)
                tmpcolor = colors.darkergrey;
            else
                tmpcolor = colors.verydarkgrey;
            end

            tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                        'fontsize',18,...
                        'units','data',...
                        'horizontalalignment','center',...
                        'verticalalignment','middle',...
                        'color',tmpcolor, ...
                        'rotation',wordangles(j), ...
                        'interpreter','latex');

        end    
    end    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add 8 descriptors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% because of axes flips, starts at top, goes clockwise

categories = { ...
    'dangerous', ...
    'dangerous-powerful',...
    'powerful',...
    'safe-powerful',...
    'safe',...
    'weak-safe',...
    'weak',...
    'weak-dangerous'...
    };

phivals = [0:45:315]*pi/180;

textangles = [0, -45, -90, 45, 0, -45, 90, 45];

for i = 1:length(phivals)

    if (mod(i,2)==1)
        tmpXcoord = 0.95*sin(phivals(i));
        tmpYcoord = 0.95*cos(phivals(i));
    else
        tmpXcoord = 1.0*sin(phivals(i));
        tmpYcoord = 1.0*cos(phivals(i));
    end
    
    if ((i<=3) | (i>=7))
        %%        tmpstr = sprintf('$\\underbrace{\\mbox{%s}}$', ...
        tmpstr = sprintf('%s', ...
                         categories{i});
    else
        %%        tmpstr = sprintf('$\\overbrace{\\mbox{%s}}$', ...
        tmpstr = sprintf('%s', ...
                         categories{i});
    end
    
        
    tmpcolor = colors.verydarkgrey;

    tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                'fontsize',24,...
                'units','data',...
                'horizontalalignment','center',...
                'color',tmpcolor, ...
                'rotation',textangles(i), ...
                'verticalalignment','middle',...
                'interpreter','latex');
    
    %% arrows
    
    if (mod(i,2)==1)
        tmpXcoord = 0.875*sin(phivals(i));
        tmpYcoord = 0.875*cos(phivals(i));
    else
        tmpXcoord = 0.925*sin(phivals(i));
        tmpYcoord = 0.925*cos(phivals(i));
    end
    
    if ((i<=3) | (i>=7))
        tmpstr = sprintf('$\\uparrow$');
    else
        tmpstr = sprintf('$\\downarrow$');
    end
    
    tmpcolor = colors.verydarkgrey;

    tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                'fontsize',24,...
                'units','data',...
                'horizontalalignment','center',...
                'color',tmpcolor, ...
                'rotation',textangles(i), ...
                'verticalalignment','middle',...
                'interpreter','latex');


end


%% change axis line width (default is 0.5)
%% set(tmpaxes(axesnum),'linewidth',2)

%% fix up tickmarks
set(gca,'xtick',[-1:.1:1]);
%% set(gca,'xticklabel',{'','',''})
set(gca,'ytick',[-1:.1:1]);
%% set(gca,'yticklabel',{'','',''})

%% the following will usually not be printed 
%% in good copy for papers
%% (except for legend without labels)

%% remove a plot from the legend
%% set(get(get(tmph,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

%% %% legend

%% tmplh = legend('stuff',...);
%% tmplh = legend('','','');
%% 
%% tmplh.Interpreter = 'latex';
%% set(tmplh,'position',get(tmplh,'position')-[x y 0 0])
%% %% change font
%% tmplh_obj = findobj(tmplh,'type','text');
%% set(tmplh_obj,'FontSize',20);
%% %% remove box:
%% legend boxoff

%% use latex interpreter for text, sans serif

clear tmpstr;
%% tmpstr = sprintf('$%s \\simeq %4.2f%s + %4.2f%s + %4.2f%s$', ...
%%                  notations.success,...
%%                  U(1,1),...
%%                  notations.valence,...
%%                  U(2,1),...
%%                  notations.arousal,...
%%                  U(3,1),...
%%                  notations.dominance);

tmpstr = sprintf('$\\leftarrow$ %s \\quad $%s$ \\quad %s $\\rightarrow$', ...
                 dimensions_new(1).negative,...
                 dimensions_new(1).notation,...
                 dimensions_new(1).positive);

tmpxlab=xlabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','top',...
               'interpreter','latex');

%% signs are dangerously messed with for better rendering
%% D's weighting is 0
%% tmpstr = sprintf('$%s \\simeq  %4.2f%s + %4.2f%s + %4.2f%s$',...
%%                  notations.danger,...
%%                  U(1,2),...
%%                  notations.valence,...
%%                  U(2,2),...
%%                  notations.arousal,...
%%                  U(3,2),...
%%                  notations.dominance);

tmpstr = sprintf('$\\leftarrow$ %s \\quad $%s$ \\quad %s $\\rightarrow$', ...
                 dimensions_new(2).negative,...
                 dimensions_new(2).notation,...
                 dimensions_new(2).positive);

tmpylab=ylabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','bottom',...
               'interpreter','latex');

%% set(tmpxlab,'position',get(tmpxlab,'position') - [0 .1 0]);
%% set(tmpylab,'position',get(tmpylab,'position') - [.1 0 0]);

%% set 'units' to 'data' for placement based on data points
%% set 'units' to 'normalized' for relative placement within axes
%% tmpXcoord = ;
%% tmpYcoord = ;
%% tmpstr = sprintf(' ');
%% or
%% tmpstr{1} = sprintf(' ');
%% tmpstr{2} = sprintf(' ');
%%
%% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%     'fontsize',24,...
%%     'units','normalized',...
%%     'horizontalalignment','left',...
%%     'verticalalignment','top',...
%%     'interpreter','latex');

%% label (A, B, ...)
%% tmplabelh = addlabel4(' A ',0.02,0.9,20);
%% tmplabelh = addlabel5(loop_i,0.02,0.9,20);
%% or:
%% tmplabelXcoord= 0.015;
%% tmplabelYcoord= 0.88;
%% tmplabelbgcolor = 0.85;
%% tmph = text(tmplabelXcoord,tmplabelYcoord,...
%%    ' A ',...
%%    'fontsize',24,
%%         'units','normalized');
%%    set(tmph,'backgroundcolor',tmplabelbgcolor*[1 1 1]);
%%    set(tmph,'edgecolor',[0 0 0]);
%%    set(tmph,'linestyle','-');
%%    set(tmph,'linewidth',1);
%%    set(tmph,'margin',1);

%% rarely used (text command is better)
%% title(' ','fontsize',24,'interpreter','latex')
%% 'horizontalalignment','left');
%% tmpxl = xlabel('','fontsize',24,'verticalalignment','top');
%% set(tmpxl,'position',get(tmpxl,'position') - [ 0 .1 0]);
%% tmpyl = ylabel('','fontsize',24,'verticalalignment','bottom');
%% set(tmpyl,'position',get(tmpyl,'position') - [ 0.1 0 0]);
%% title('','fontsize',24)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% automatic creation of image file (pdf, postscript, png, ...)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% for pdf, without name/date
imageformat.type = 'pdf';
imageformat.dpi = 600;
imageformat.deleteps = 'yes';
imageformat.open = 'yes';
imageformat.copylink = 'no';

print_universal(tmpfilenoname,imageformat);

%% name label
%% tmpt = pwd;
%% tmpnamememo = sprintf('[source=%s/%s.ps]',tmpt,tmpfilename);
%% 
%% [tmpXcoord,tmpYcoord] = normfigcoords(1.05,.05);
%% tmph = text(tmpXcoord,tmpYcoord,tmpnamememo,...
%%      'units','normalized',...
%%      'fontsize',2,...
%%      'rotation',90,'color',0.8*[1 1 1]);

%% [tmpXcoord,tmpYcoord] = normfigcoords(1.1,.05);
%% datenamer(tmpXcoord,tmpYcoord,90);

%% automatic creation of postscript
%% psprintcpdf(tmpfilename);

%% now in print_universal
%% tmpcommand = sprintf('open %s.pdf;',tmpfilenoname);
%% system(tmpcommand);

%% now in print_universal
%% tmpcommand = sprintf('printf ''%s.pdf'' | pbcopy',tmpfilenoname);
%% system(tmpcommand);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %% png with name/date
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% axesnum = 1;
%% axes(tmpaxes(axesnum));
%% %% tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);
%% 
%% clear tmpstr;
%% tmpstr = sprintf(' otcompstorylab');
%% %% dates covered: %s to %s', ... 
%% %%                 datestr(startdatenum,'yyyy/mm/dd'), ...
%% %%                 datestr(datenum2,'yyyy/mm/dd'));
%% 
%% tmpXcoord = 0.00;
%% tmpYcoord = 0.00;
%% 
%% text(tmpXcoord,tmpYcoord,tmpstr,...
%%      'fontsize',16,...
%%      'units','normalized',...
%%      'horizontalalignment','left',...
%%      'verticalalignment','bottom',...
%%      'rotation',0,...
%%      'color',0.7*[1 1 1],...
%%      'interpreter','latex')
%% 
%% print_universal(tmpfiletwittername,imageformat);
%% 
%% tmpcommand = sprintf('convert -background white -flatten -geometry 1600x -density 600 %s.pdf %s.png', ...
%%                      tmpfilesharename, ...
%%                      tmpfilesharename);
%% system(tmpcommand);
%% 
%% tmpcommand = sprintf('permanentshare-uvm.pl %s.png', ...
%%                      tmpfiletwittername);
%% system(tmpcommand);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% archivify (0 = off, non-zero = on)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

archiveswitch = 0;
figarchivify(tmpfilenoname,archiveswitch);

%% prevent hidden figure clutter/bloat
%% may need to switch this off for some test
close(tmpfigh);

%% clean up tmp* files
clear tmp*

more on;
