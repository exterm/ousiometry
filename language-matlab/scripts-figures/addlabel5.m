function tmph = addlabel5(alphaindex,labelXcoord,labelYcoord,fontsize)
%% tmph = addlabel5(alphaindex,labelXcoord,labelYcoord,fontsize)
%%
%% add label to current plot
%% alphaindex points to capital letter 
%% 1 -> A, 2 -> B, ...  
%% 
%% obviously works well up to Z only.
%% 
%% uses normalized units
%% uses main figure's font 

label = sprintf('\\ %s ',char('A'+alphaindex-1));
labelbgcolor = 0.85*[1 1 1];

tmph = text(labelXcoord,labelYcoord,...
            label,...
            'Fontsize',fontsize,...
            'units','normalized',...
            'backgroundcolor',labelbgcolor,...
            'edgecolor',[0 0 0],...
            'linestyle','-',...
            'linewidth',0.5,...
            'interpreter','latex',...
            'margin',0.5);

%%            'backgroundcolor','none',...
