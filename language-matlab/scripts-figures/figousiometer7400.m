function things = figousiometer7400(types, ...
                                    token_frequencies, ...
                                    type_dimension_weights, ...
                                    dimension_settings, ...
                                    general_settings)
%% 
%% things = figousiometer7400(types, ...
%%                                    token_frequencies, ...
%%                                    type_dimension_weights, ...
%%                                    dimension_settings, ...
%%                                    general_settings)
%% 
%% generalized version of figousiometer7300
%% 
%% types = cell with names of types (Ntypes x 1)
%% 
%% type_dimension_weights = weighting in each direction (Ntypes x 2)
%% 
%% dimension_settings = names, annotations for labels, ranges
%% 
%% set token_frequencies to all ones for a lexicon presentation
%% 
%% 
%% minor output:
%% 
%% things = structure for outputs of possible use



%%%%%%%%%%%%%%%%%%%%%%%%
%% process settings
%%%%%%%%%%%%%%%%%%%%%%%%

%% remnant of PDS:
indexpair = [1 2];
dim1 = 1;
dim2 = 2;

if (~isfield(general_settings,'filetag'))
    error('need a tag set for file name');
else
    filetag = general_settings.filetag;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations through the middle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% not used yet

if (~isfield(general_settings,'Nannotations_horizontal'))
    
else
    Nannotations_horizontal = general_settings.Nannotations_horizontal;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% preliminary handling of counts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

token_frequencies = col(token_frequencies);
zipfindices = find(token_frequencies > 0);
totaltoken_frequencies = sum(token_frequencies);

zipftypes = types(zipfindices);
%% using rows here (remnants of PDS)
type_ousiometric_avgs = ...
    transpose(type_dimension_weights(zipfindices,:));

%% normalize with room:
%% type_ousiometric_avgs =  type_ousiometric_avgs * 0.5 ...
%%     / max(abs(type_ousiometric_avgs(:)));

typefreqs = token_frequencies(zipfindices);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% create ordered distributions for statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:2
    [~,indices] = sort(type_ousiometric_avgs(i,:),'ascend');
    sortedavgs(i,1:length(zipftypes)) = row(type_ousiometric_avgs(i,indices));
    freqs(i,1:length(zipftypes)) = row(typefreqs(indices));
    probs(i,1:length(zipftypes)) = row(typefreqs(indices)/sum(typefreqs(indices)));
    truemedians(i) = sortedavgs(i,min(find(cumsum(probs(i,:)) >= 0.5)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% figure building 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

more off;

%%% 

loadcolors;

%%% 


figure('visible','off');
set(gcf,'color','none');
tmpfigh = gcf;


clf;
figshape(1000,1000);
%% automatically create postscript whenever
%% figure is drawn
tmpfilename = 'figousiometer7400';
tmpcommand = sprintf('mkdir -p %s',tmpfilename);
system(tmpcommand);

tmpfilenoname = sprintf('%s/%s_%s_noname',tmpfilename,tmpfilename,filetag);
tmpfilesharename = sprintf('%s_',tmpfilename);

%% global switches

set(gcf,'Color','none');
set(gcf,'InvertHardCopy', 'off');

set(gcf,'DefaultAxesFontname','times');
set(gcf,'Renderer','Painters');

set(gcf,'DefaultAxesColor','none');
set(gcf,'DefaultLineMarkerSize',10);
% set(gcf,'DefaultLineMarkerEdgeColor','k');
set(gcf,'DefaultLineMarkerFaceColor','w');
set(gcf,'DefaultAxesLineWidth',0.5);

set(gcf,'PaperPositionMode','auto');

%% tmpsym = {'ok-','sk-','dk-','vk-','^k-','>k-','<k-','pk-','hk-'};
%% tmpsym = {'k-','r-','b-','m-','c-','g-','y-'};
%% tmpsym = {'k-','k-.','k:','k--','r-','r-.','r:','r--'};
%% tmplw = [ 1.5*ones(1,4), .5*ones(1,4)];


%% one simple plot
axes_positions(1).box = [.20 .20 .70 .70];

%% %% create an array of plots
%% 
%% xoffset = 0.05;
%% yoffset = 0.05; 
%% width = .30;
%% height = .30; 
%% xsep = 0.02;
%% ysep = 0.02;
%% numrows = 1; 
%% numcols = 3;
%% axes_positions = makeaxes_positions_grid(xoffset,...
%%                             yoffset,...
%%                             width,...
%%                             height,...
%%                             xsep,...
%%                             ysep,...
%%                             numrows,...
%%                             numcols);

%% %% create the same inset axis for each axis
%% 
%% 
%%  relative_xoffset = 0.10;
%%  relative_yoffset = 0.70;
%%  relative_width = 0.20;
%%  relative_height = 0.20;
%% 
%% inset_positions = makeaxes_inset_positions(...
%%    axes_positions,...
%%    relative_xoffset,...
%%    relative_yoffset,...
%%    relative_width,...
%%    relative_height)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% histogram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (~isfield(general_settings,'binsize'))
    binsize = 0.1/1;
else
    binsize = general_settings.binsize;
end


binsize

if (isfield(dimension_settings(1),'range'))
    xbinedges = [dimension_settings(1).range(1):binsize: ...
                 dimension_settings(1).range(2)]';
else
    xbinedges = [-1:binsize:1]';
end

if (isfield(dimension_settings(2),'range'))
    ybinedges = [dimension_settings(2).range(1):binsize: ...
                 dimension_settings(2).range(2)]';
else
    ybinedges = [-1:binsize:1]';
end

edges{1} = xbinedges;
edges{2} = ybinedges;

if (max(type_ousiometric_avgs(1,1) > ...
        dimension_settings(1).range(2)))
    error('Max of first variable outside of specified range');
elseif (max(type_ousiometric_avgs(2,1) > ...
        dimension_settings(2).range(2)))
    error('Max of second variable outside of specified range');
elseif (min(type_ousiometric_avgs(1,1) < ...
        dimension_settings(1).range(1)))
    error('Min of first variable outside of specified range');
elseif (min(type_ousiometric_avgs(2,1) < ...
        dimension_settings(2).range(1)))
    error('Min of second variable outside of specified range');
end

%% Nx = (length(xbinedges) - 1)/2;
%% Ny = (length(ybinedges) - 1)/2;


%% underlying lexicon (not crucial but shape is used, residual
%% code)

%% whos type_ousiometric_avgs

[binnedcounts,centers] = hist3(type_ousiometric_avgs(indexpair,:)',...
                         'edges',edges);

%% marginals for underlying lexicon

xbinned_counts = sum(binnedcounts,2);
ybinned_counts = sum(binnedcounts,1);

%% actual counts (may still be lexicon)
binned_token_frequencies = zeros(size(binnedcounts));

%% zipf distribution
for i=1:length(zipfindices)
    index_1 = 1 + floor((type_ousiometric_avgs(dim1,i) - dimension_settings(1).range(1)) / binsize);
    index_2 = 1 + floor((type_ousiometric_avgs(dim2,i) - dimension_settings(2).range(1)) / binsize);
    binned_token_frequencies(index_1,index_2) = ...
        binned_token_frequencies(index_1,index_2) + token_frequencies(zipfindices(i));
end

%% marginals

xbinned_token_frequencies = sum(binned_token_frequencies,2);
ybinned_token_frequencies = sum(binned_token_frequencies,1);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% main plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

axesnum = 1;
tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);




%% color map
if (isfield(general_settings,'colormaptype')) 
    if (strcmp(general_settings.colormaptype,'normalized'))
        overallmaxcounts = 100;
    end
elseif (~isfield(general_settings,'overallmaxcounts'))
    overallmaxcounts = 100;
    general_settings.colormaptype = 'normalized';
else 
    overallmaxcounts = general_settings.overallmaxcounts;
    general_settings.colormaptype = 'indexed';
end
%% use part of magma
heatmapcolors = flipud(magma(3*overallmaxcounts));

things.maxcolorindex_data = max(binned_token_frequencies(:));

if (strcmp(general_settings.colormaptype,'indexed'))
    for i=1:size(binned_token_frequencies,1)-1
        for j=1:size(binned_token_frequencies,2)-1
            if (binned_token_frequencies(i,j) > 0)
                tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                tmph = rectangle('position',tmppos);
                hold on;
                
                colorindex = binned_token_frequencies(i,j);
                if (colorindex > overallmaxcounts)
                    fprintf(1,'colorindex = %d, above %d\n', ...
                            colorindex, ...
                            overallmaxcounts);
                    colorindex = overallmaxcounts;
                end
                set(tmph,'facecolor',heatmapcolors(colorindex,:));
                set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                
                
            end
        end
    end
else
    for i=1:size(binned_token_frequencies,1)-1
        for j=1:size(binned_token_frequencies,2)-1
            if (binned_token_frequencies(i,j) > 0)
                tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                tmph = rectangle('position',tmppos);
                hold on;
                
                colorindex = 1+floor(0.999*overallmaxcounts*binned_token_frequencies(i,j) / max(binned_token_frequencies(:)));
                set(tmph,'facecolor',heatmapcolors(colorindex,:));
                set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                
                
            end
        end
    end
end


set(gca,'color',colors.lightergrey);
set(gca,'fontsize',18)
%% axis([]);
% xlim([-.3 1.4]);
% ylim([-.2 1.5]);

%% xlim([-.85 .85]);
%% ylim([-.85 .85]);

xlim(dimension_settings(1).range);
ylim(dimension_settings(2).range);


grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

colorbarpos = [-.975 +0.65];

addcolorbar7400;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% marginals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xbinned_token_frequencies = sum(binned_token_frequencies,2);
ybinned_token_frequencies = sum(binned_token_frequencies,1);

offset = 0.03;

%% maxmarginalbinnedcounts = 300;

set(gca,'clipping','off');

if (isfield(general_settings,'marginal_dist_height')) 
    marginal_dist_height = general_settings.marginal_dist_height;
else
    marginal_dist_height = 2;
end

for i=1:size(xbinned_token_frequencies,1)-1
    if (xbinned_token_frequencies(i) > 0)
        height = marginal_dist_height*binsize * xbinned_token_frequencies(i) / max(xbinned_token_frequencies);
        %%        tmppos = [xbinedges(i), 1+offset, binsize, binsize];
        %%        tmppos = [xbinedges(i), -1 + offset + 2*binsize - height/2, binsize, height];
        tmppos = [xbinedges(i), dimension_settings(2).range(2) + 0*offset, binsize, height];
        tmph = rectangle('position',tmppos);
        hold on;
        
        %%         colorindex = 1+round(maxmarginalcounts* ...
        %%                              xbinned_token_frequencies(i)/max(xbinned_token_frequencies));
        %%         set(tmph,'facecolor',heatmapcolors(colorindex,:));
        %%         set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));

        %% 
        %%        set(tmph,'edgecolor',colors.lightgreyish);
        
        if (xbinedges(i) >= 0)
            set(tmph,'facecolor',colors.grey);
        else
            set(tmph,'facecolor',colors.lightgrey);
        end
        set(tmph,'edgecolor',colors.darkgrey);
    
    end
end

for i=1:size(ybinned_token_frequencies,2)-1
    if (ybinned_token_frequencies(i) > 0)

        width = marginal_dist_height*binsize * ybinned_token_frequencies(i) / max(ybinned_token_frequencies);
        tmppos = [dimension_settings(1).range(2) + 0*offset, ybinedges(i), width, binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        %%        colorindex = 1+round(maxmarginalcounts*ybinned_token_frequencies(i)/max(ybinned_token_frequencies));
        %%        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        %%        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        
        %%        set(tmph,'facecolor',colors.lightgreyer);
        %%        set(tmph,'edgecolor',colors.lightgreyish);
    
        if (ybinedges(i) >= 0)
            set(tmph,'facecolor',colors.grey);
        else
            set(tmph,'facecolor',colors.lightgrey);
        end
        set(tmph,'edgecolor',colors.darkgrey);

    end
end

%% add indicators for medians

xmedian = truemedians(dim1);
ymedian = truemedians(dim2);

tmph = plot(xmedian, dimension_settings(2).range(2) + 0.02,'v');
set(tmph,'markerfacecolor',colors.verydarkgrey);
set(tmph,'markeredgecolor',colors.verydarkgrey);
set(tmph,'markersize',10);

tmph = plot(dimension_settings(1).range(2) + 0.02,ymedian,'<');
set(tmph,'markerfacecolor',colors.verydarkgrey);
set(tmph,'markeredgecolor',colors.verydarkgrey);
set(tmph,'markersize',10);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmpXcoord = mean(dimension_settings(1).range);
tmpYcoord = dimension_settings(2).range(2) * 1.08;

if (isfield(general_settings,'titleinsert'))
    general_settings.title = sprintf('$\\sim$ %s-%s ousiogram for %s $\\sim$',...
                             dimension_settings(dim1,coordsindex).namelc, ...
                             dimension_settings(dim2,coordsindex).namelc, ...
                             general_settings.titleinsert);
elseif (~isfield(general_settings,'title'))
    general_settings.title = sprintf('$\\sim$ ousiogram for %s-%s space $\\sim$',...
                             dimension_settings(dim1,coordsindex).namelc, ...
                             dimension_settings(dim2,coordsindex).namelc);
end
tmpstr = general_settings.title;

tmpcolor = colors.verydarkgrey;
tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
            'fontsize',26,...
            'units','data',...
            'horizontalalignment','center',...
            'verticalalignment','middle',...
            'color',tmpcolor, ...
            'interpreter','latex');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% lexicon size, total type count
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmpXcoord = -0.96;
tmpYcoord = -0.98;
clear tmpstr;

if (isfield(general_settings,'Ntypes')) 
    tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
                        addcommas(general_settings.Ntypes));
else
    tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
                        addcommas(sum(token_frequencies>0)));
end

if (isfield(general_settings,'Ntokens')) 
    tmpstr{2} = sprintf('$N_{\\rm types}$ = %s', ...
                        addcommas(general_settings.Ntokens));
else
    tmpstr{2} = sprintf('$N_{\\rm types}$ = %s', ...
                        addcommas(sum(token_frequencies)));
end

tmpcolor = colors.darkergrey;
tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
            'fontsize',16,...
            'units','data',...
            'horizontalalignment','left',...
            'verticalalignment','bottom',...
            'color',tmpcolor, ...
            'interpreter','latex');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add optional svd fit: axes and ellipse for underlying lexicon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(general_settings,'showellipse') & strcmp(general_settings.showellipse,'yes')==1)

    Upair = ousiometry_data(coordsindex).svd_data(indexpair_i).Upair;
    sigmaspair = ousiometry_data(coordsindex).svd_data(indexpair_i).sigmaspair;

    slope = Upair(2,1)/Upair(1,1);
    angle = atan(slope);

    %% axes

    maxhalfwidth = 0.45;
    tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
    tmpr1 = sigmaspair(1)/sigmaspair(1);
    tmpx = tmpt*tmpr1*cos(angle);
    tmpy = tmpt*tmpr1*sin(angle);

    tmph = plot(tmpx,tmpy,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);


    slope = Upair(2,2)/Upair(1,2);
    angle = atan(slope);

    tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
    tmpr2 = sigmaspair(2)/sigmaspair(1);
    tmpx = tmpt*tmpr2*cos(angle);
    tmpy = tmpt*tmpr2*sin(angle);

    tmph = plot(tmpx,tmpy,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);
    
    %% add diagonals
    if (axesnum==7)
        maxhalfwidth = 0.45;
        tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
        tmpr1 = sigmaspair(1)/sigmaspair(1);
        tmpx = tmpt*tmpr1*cos(pi/4);
        tmpy = tmpt*tmpr1*sin(pi/4);

        tmph = plot(tmpx,tmpy,'k-');
        set(tmph,'linewidth',0.5);
        set(tmph,'color',colors.verydarkgrey);
        
        maxhalfwidth = 0.45;
        tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
        tmpr1 = sigmaspair(1)/sigmaspair(1);
        tmpx = tmpt*tmpr1*cos(pi/4);
        tmpy = tmpt*tmpr1*sin(-pi/4);

        tmph = plot(tmpx,tmpy,'k-');
        set(tmph,'linewidth',0.5);
        set(tmph,'color',colors.verydarkgrey);
        
    end

    %% ellipse

    tmprotation = [cos(angle) -sin(angle); sin(angle) cos(angle)];

    tmptheta = linspace(0,2*pi,100);
    tmpx = maxhalfwidth*tmpr2*cos(tmptheta);
    tmpy = maxhalfwidth*tmpr1*sin(tmptheta);

    tmpxrot = tmprotation(1,:) * [tmpx; tmpy];
    tmpyrot = tmprotation(2,:) * [tmpx; tmpy];

    tmph = plot(tmpxrot,tmpyrot,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);

    if ((coordsindex==3) & (indexpair_i==1))
                set(tmph,'linestyle','--');
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

annotatedtypes = zeros(length(zipftypes),1);

tmpoffset = 0.02;

%% defaults
annotation_settings.binwidth = 1/20;
annotation_settings.fontsize = 20;
annotation_settings.breakangle = 3/4 * pi;
annotation_settings.textoffset = 0.05;

if (isfield(general_settings,'do_not_annotate')) 
    annotatedtypeslist = general_settings.do_not_annotate;
else
    clear annotatedtypeslist;
    annotatedtypeslist{1} = 'woifjwoijfweof';
end

%% set in ousiometry general_settings
annotation_settings.excludedtypes = annotatedtypeslist;

%% override if set:
if (isfield(general_settings,'annotation_settings'))
    if (isfield(general_settings.annotation_settings,'binwidth'))
        annotation_settings.binwidth = ...
            general_settings.annotation_settings.binwidth;
    end
    if (isfield(general_settings.annotation_settings,'linefactor'))
        annotation_settings.linefactor = ...
            general_settings.annotation_settings.linefactor;
    end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: medians on eight paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% annotate moving out from center
%% avoid some number of central types
%% 
%% default number of annotations from the edge is 7
%% 
%% todo: make number of types in each dimension adjustable
%% todo: prevent types from reprinting



if (isfield(general_settings,'Nmedian_start')) 
    Nmedian_start = general_settings.Nmedian_start;
else
    Nmedian_start = 5;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: along each eight paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% possible todo
%% general_settings.annotation_angles

if (isfield(general_settings,'annotate_internally'))
    if (strcmp(general_settings.annotate_internally,'yes'))
        xvals = type_ousiometric_avgs(indexpair(1),:);
        yvals = type_ousiometric_avgs(indexpair(2),:);

        phivals = [0:45:315]*pi/180;

        phi1 = 45;
        textangles = [0, -phi1, -90, phi1, 0, -phi1, 90, phi1];


        for i_phi = 0:-1; %1:length(phivals)
            phi = phivals(i_phi)

            Rotation = [
                cos(phi) -sin(phi);
                sin(phi) cos(phi)
                       ];

            xyvals = [row(xvals); row(yvals)];
            xyvals_rot = Rotation*xyvals;

            xvals_rot = xyvals_rot(1,:);
            yvals_rot = xyvals_rot(2,:);

            %% back to normal 2-d view

            ymin_rot = min(yvals_rot);
            ymax_rot = max(yvals_rot);

            %% bin along the positive vertical axis
            nmin = 0; %% center
            nmax = ceil(ymax_rot/annotation_settings.binwidth);

            binedges = (nmin:1:nmax)'*annotation_settings.binwidth;
            bincenters = binedges(1:end-1) + annotation_settings.binwidth/2;


            %% todo: add more options here
            if (isfield(general_settings,'annotation_type')) 
                if(strcmp(general_settings.annotation_type,'outer'))
                    startindex = Nmedian_start;
                else
                    error('Annotation type must be one of: outer (oops)');
                end
            else
                %% annotate center column fully
                if (i_phi == 1)
                    startindex = 1;
                elseif (i_phi == 5)
                    startindex = 1;
                elseif (mod(i_phi,2)==1)
                    startindex = Nmedian_start;
                else 
                    startindex = Nmedian_start + 1;
                end
            end
            
            for bin_i = startindex:length(binedges)-1
                
                binindices = find((yvals_rot >= binedges(bin_i)) & ...
                                  (yvals_rot < binedges(bin_i + 1)));

                %% tight to center line
                
                %% ignoring third dimension
                [minval,index] = min(abs(xyvals_rot(1,binindices)));
                
                if ((length(index)>0) & (minval < 3*annotation_settings.binwidth))
                    type = zipftypes{binindices(index)};

                    if (~strcmp(annotatedtypeslist,type)) %% unless already annotated
                        
                        annotatedtypeslist{end+1} = type;
                        
                        tmpXcoord_rot = 0;
                        tmpYcoord_rot = binedges(bin_i) + annotation_settings.binwidth/2;

                        tmpXYcoords = inv(Rotation)*[tmpXcoord_rot; tmpYcoord_rot];
                        tmpXcoord = tmpXYcoords(1);
                        tmpYcoord = tmpXYcoords(2);
                        
                        tmpstr = sprintf('%s',type);
                        %% fix quotes
                        tmpstr = regexprep(tmpstr,' ''',' `');
                        tmpstr = regexprep(tmpstr,'^''','`');

                        if (mod(bin_i,2)==1)
                            tmpcolor = colors.darkergrey;
                        else
                            tmpcolor = colors.verydarkgrey;
                        end

                        tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                                    'fontsize',18,...
                                    'units','data',...
                                    'horizontalalignment','center',...
                                    'color',tmpcolor, ...
                                    'rotation',textangles(i_phi), ...
                                    'verticalalignment','middle',...
                                    'interpreter','latex');
                    end
                end
            end
        end
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: convex hull
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



annotatedtypeslist = annotate_histogram_convexhull_types001( ...
    type_ousiometric_avgs(dim1,:)', ...
    type_ousiometric_avgs(dim2,:)', ...
    zipftypes, ...
    annotation_settings);

%% annotatedtypeslist
%% error(' ')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: overlaid types
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(isfield(general_settings,'do_annotate'))

    if(isfield(general_settings.do_annotate,'types'))
        
        for i=1:length(general_settings.do_annotate.types)
            i;
            type = general_settings.do_annotate.types{i};
            index = find(strcmp(zipftypes,type));
            
            if (mod(i,2)==1)
                tmpcolor = colors.darkgrey;
            else
                tmpcolor = colors.verydarkgrey;
            end

            if(strcmp(general_settings.do_annotate.kind,'polar'))
                
                if(isfield(general_settings.do_annotate,'altvalues'))
                    tmpXcoord1 = general_settings.do_annotate.altvalues(i,1);
                    tmpYcoord1 = general_settings.do_annotate.altvalues(i,2);
                else
                    tmpXcoord1 = type_ousiometric_avgs(indexpair(1),index);
                    tmpYcoord1 = type_ousiometric_avgs(indexpair(2),index);
                end

                %% add line
                
                %% add marker
                
                plot(tmpXcoord1,tmpYcoord1,'ko');

                %% add type
                tmpangle = atan2(tmpYcoord1,tmpXcoord1) + general_settings.do_annotate.offset_angles(i)
                
                if (abs(tmpangle) < pi/2)
                    tmpangle_degrees = 180/pi*tmpangle;
                    tmphalign = 'left';
                else 
                    tmpangle_degrees = 180/pi*(tmpangle + pi);
                    tmphalign = 'right';
                end
                
                tmpoffset = 0.025;
                tmpXcoord = tmpXcoord1 + tmpoffset*cos(tmpangle);
                tmpYcoord = tmpYcoord1 + tmpoffset*sin(tmpangle);
                tmpstr = sprintf(' %s ',type);
                %% fix quotes
                tmpstr = regexprep(tmpstr,' ''',' `');
                tmpstr = regexprep(tmpstr,'^''','`');
                tmpstr

                tmph = text(tmpXcoord,tmpYcoord,tmpstr, ...
                            'fontsize',24, ...
                            'units','data', ...
                            'color',tmpcolor, ...
                            'rotation',tmpangle_degrees,...
                            'horizontalalignment',tmphalign, ...
                            'verticalalignment','middle', ...
                            'interpreter','latex');
                
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% categories 
%% 8 compass points
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Todo:
%% change to annotation category type:
%% 'none'
%% 'custom'
%% 'VAD/GES/PDS'

%% trickiness: matrices are flips of how we want them plotted
%% clockwise, starting from the top of the clock


if (isfield(general_settings,'do_not_annotate_categories'))
    annotate_plain_categories = 0;
else
    annotate_plain_categories = 1;
end

if (annotate_plain_categories == 1)
    k=0;
    k=k+1;
    categories{k} = dimension_settings(dim2,coordsindex).positive1type;
    k=k+1;
    categories{k} = sprintf('%s-%s', ...
                            dimension_settings(dim2,coordsindex).positive1type, ...
                            dimension_settings(dim1,coordsindex).positive1type);
    k=k+1;
    categories{k} = dimension_settings(dim1,coordsindex).positive1type;
    k=k+1;
    categories{k} = sprintf('%s-%s', ...
                            dimension_settings(dim2,coordsindex).negative1type, ...
                            dimension_settings(dim1,coordsindex).positive1type);
    k=k+1;
    categories{k} = dimension_settings(dim2,coordsindex).negative1type;
    k=k+1;
    categories{k} = sprintf('%s-%s', ...
                            dimension_settings(dim2,coordsindex).negative1type, ...
                            dimension_settings(dim1,coordsindex).negative1type);
    k=k+1;
    categories{k} = dimension_settings(dim1,coordsindex).negative1type;
    k=k+1;
    categories{k} = sprintf('%s-%s', ...
                            dimension_settings(dim2,coordsindex).positive1type, ...
                            dimension_settings(dim1,coordsindex).negative1type);
end


if (isfield(general_settings,'do_annotate'))
    if (isfield(general_settings.do_annotate,'categories'))
        annotate_plain_categories = 0;
        categories = general_settings.do_annotate.categories;
    end

    phivals = [0:45:315]*pi/180;

    phi1 = 45;
    textangles = [0, -phi1, -90, phi1, 0, -phi1, 90, phi1];

    widths = std(type_ousiometric_avgs,[],2);

    for i = 1:length(phivals)

        %%     ellipticity = 1; %% 0.925;
        %%     
        %%     tmpXcoord = 0.95*sin(phivals(i));
        %%     tmpYcoord = ellipticity*0.95*cos(phivals(i));

        if (mod(i,2)==1)
            tmpXcoord = 0.950*sin(phivals(i));
            tmpYcoord = 0.950*cos(phivals(i));
        else
            tmpXcoord = 1.000*sin(phivals(i));
            tmpYcoord = 1.000*cos(phivals(i));
        end
        
        
        clear tmpstr;
        tmpstr = sprintf('{%s}', ...
                         categories{i});
        
        tmpcolor = colors.superdarkgrey;

        tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                    'fontsize',24,...
                    'units','data',...
                    'horizontalalignment','center',...
                    'color',tmpcolor, ...
                    'rotation',textangles(i), ...
                    'verticalalignment','middle',...
                    'interpreter','latex');

        
        %% arrows
        
        %%     ellipticity = 1; %% 0.85;
        %%     tmpXcoord = 0.875*sin(phivals(i));
        %%     tmpYcoord = ellipticity*0.875*cos(phivals(i));

        if (mod(i,2)==1)
            tmpXcoord = 0.875*sin(phivals(i));
            tmpYcoord = 0.875*cos(phivals(i));
        else
            tmpXcoord = 0.925*sin(phivals(i));
            tmpYcoord = 0.925*cos(phivals(i));
        end
        
        
        
        if ((i<=3) | (i>=7))
            tmpstr = sprintf('${\\uparrow}$');
        else
            tmpstr = sprintf('${\\downarrow}$');
        end
        
        tmpcolor = colors.superdarkgrey;

        tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                    'fontsize',24,...
                    'units','data',...
                    'horizontalalignment','center',...
                    'color',tmpcolor, ...
                    'rotation',textangles(i), ...
                    'verticalalignment','middle',...
                    'interpreter','latex');
    end    
end


%% change axis line width (default is 0.5)
%% set(tmpaxes(axesnum),'linewidth',2)

%% fix up tickmarks

%% TODO: make configurable

%% set(gca,'xtick',[-1:.1:1]);
%% set(gca,'xticklabel',{'','',''})
%% set(gca,'ytick',[-1:.1:1]);
%% set(gca,'yticklabel',{'','',''})

%% the following will usually not be printed 
%% in good copy for papers
%% (except for legend without labels)

%% remove a plot from the legend
%% set(get(get(tmph,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

%% %% legend

%% tmplh = legend('stuff',...);
%% tmplh = legend('','','');
%% 
%% tmplh.Interpreter = 'latex';
%% set(tmplh,'position',get(tmplh,'position')-[x y 0 0])
%% %% change font
%% tmplh_obj = findobj(tmplh,'type','text');
%% set(tmplh_obj,'FontSize',20);
%% %% remove box:
%% legend boxoff

%% use latex interpreter for text, sans serif

clear tmpstr;

if (~isfield(general_settings,'horizontal_axis_label'))
    tmpstr = sprintf('$\\leftarrow$ %s \\quad %s $%s$ \\quad %s $\\rightarrow$', ...
                     dimension_settings(dim1).negative,...
                     dimension_settings(dim1).namelc,...
                     dimension_settings(dim1).notation,...
                     dimension_settings(dim1).positive);
else
    tmpstr = general_settings.horizontal_axis_label;
end


tmpxlab=xlabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','top',...
               'interpreter','latex');



if (~isfield(general_settings,'vertical_axis_label'))
    tmpstr = sprintf('$\\leftarrow$ %s \\quad %s $%s$ \\quad %s $\\rightarrow$', ...
                     dimension_settings(dim2).negative,...
                     dimension_settings(dim2).namelc,...
                     dimension_settings(dim2).notation,...
                     dimension_settings(dim2).positive);
else
    tmpstr = general_settings.vertical_axis_label;
end



tmpylab=ylabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','bottom',...
               'interpreter','latex');

%% set(tmpxlab,'position',get(tmpxlab,'position') - [0 .1 0]);
%% set(tmpylab,'position',get(tmpylab,'position') - [.1 0 0]);

%% set 'units' to 'data' for placement based on data points
%% set 'units' to 'normalized' for relative placement within axes
%% tmpXcoord = ;
%% tmpYcoord = ;
%% tmpstr = sprintf(' ');
%% or
%% tmpstr{1} = sprintf(' ');
%% tmpstr{2} = sprintf(' ');
%%
%% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%     'fontsize',24,...
%%     'units','normalized',...
%%     'horizontalalignment','left',...
%%     'verticalalignment','top',...
%%     'interpreter','latex');

%% label (A, B, ...)
%% tmplabelh = addlabel4(' A ',0.02,0.9,20);
%% tmplabelh = addlabel5(loop_i,0.02,0.9,20);
%% or:
%% tmplabelXcoord= 0.015;
%% tmplabelYcoord= 0.88;
%% tmplabelbgcolor = 0.85;
%% tmph = text(tmplabelXcoord,tmplabelYcoord,...
%%    ' A ',...
%%    'fontsize',24,
%%         'units','normalized');
%%    set(tmph,'backgroundcolor',tmplabelbgcolor*[1 1 1]);
%%    set(tmph,'edgecolor',[0 0 0]);
%%    set(tmph,'linestyle','-');
%%    set(tmph,'linewidth',1);
%%    set(tmph,'margin',1);

%% rarely used (text command is better)
%% title(' ','fontsize',24,'interpreter','latex')
%% 'horizontalalignment','left');
%% tmpxl = xlabel('','fontsize',24,'verticalalignment','top');
%% set(tmpxl,'position',get(tmpxl,'position') - [ 0 .1 0]);
%% tmpyl = ylabel('','fontsize',24,'verticalalignment','bottom');
%% set(tmpyl,'position',get(tmpyl,'position') - [ 0.1 0 0]);
%% title('','fontsize',24)



%% %% tmph = plot(0,0,'ko');
%% %% set(tmph,'markerfacecolor',colors.darkgrey);
%% %% set(tmph,'markeredgecolor',colors.verydarkgrey);
%% 
%% clear tmpstr;
%% tmpstr{1} = sprintf('$%s \\sim %4.2f %s$', ...
%%                     dimension_settings_old(3).notation,...
%%                     slope,...
%%                     dimension_settings_old(2).notation);
%% tmpstr{2} = sprintf('$r \\simeq %4.2f$', ...
%%                     corrsVAD(2,3));
%% 
%% tmpXcoord = 0.20;
%% tmpYcoord = 0;
%% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%             'fontsize',20,...
%%             'units','data',...
%%             'horizontalalignment','center',...
%%             'color',tmpcolor, ...
%%             'rotation',0, ...
%%             'verticalalignment','middle',...
%%             'interpreter','latex');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% automatic creation of image file (pdf, postscript, png, ...)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(general_settings,'imageformat'))
    if (isfield(general_settings.imageformat,'open'))
        imageformat.open = general_settings.imageformat.open;
    else
        imageformat.open = 'yes';
    end
else
    imageformat.open = 'yes';
end

%% for pdf, without name/date
imageformat.type = 'pdf';
imageformat.dpi = 600;
imageformat.deleteps = 'yes';
%% imageformat.open = 'yes';
imageformat.copylink = 'no';

print_universal(tmpfilenoname,imageformat);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %% png with name/date
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% axesnum = 1;
%% axes(tmpaxes(axesnum));
%% %% tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);
%% 
%% clear tmpstr;
%% tmpstr = sprintf(' otcompstorylab');
%% %% dates covered: %s to %s', ... 
%% %%                 datestr(startdatenum,'yyyy/mm/dd'), ...
%% %%                 datestr(datenum2,'yyyy/mm/dd'));
%% 
%% tmpXcoord = 0.00;
%% tmpYcoord = 0.00;
%% 
%% text(tmpXcoord,tmpYcoord,tmpstr,...
%%      'fontsize',16,...
%%      'units','normalized',...
%%      'horizontalalignment','left',...
%%      'verticalalignment','bottom',...
%%      'rotation',0,...
%%      'color',0.7*[1 1 1],...
%%      'interpreter','latex')
%% 
%% print_universal(tmpfiletwittername,imageformat);
%% 
%% tmpcommand = sprintf('convert -background white -flatten -geometry 1600x -density 600 %s.pdf %s.png', ...
%%                      tmpfilesharename, ...
%%                      tmpfilesharename);
%% system(tmpcommand);
%% 
%% tmpcommand = sprintf('permanentshare-uvm.pl %s.png', ...
%%                      tmpfiletwittername);
%% system(tmpcommand);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% archivify (0 = off, non-zero = on)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

archiveswitch = 0;
figarchivify(tmpfilenoname,archiveswitch);


%% prevent hidden figure clutter/bloat
%% may need to switch this off for some test
close(tmpfigh);

%% clean up tmp* files
clear tmp*

more on;

