function things = figousiometer9001(zipfcounts,settings)
%% 
%% things = figousiometer9001(zipfcounts,settings)
%% 
%% set zipfcounts to all ones for a lexicon presentation
%% 
%% things (minor) = structure for outputs of possible use

error('Not functional ...');

load_ousiometry_settings_9001;

load ousiometry_data_SPS.mat;
%% load('~/matlab/ousiometry/data/ousiometer/ousiometry_data.mat');
%% load('~/matlab/ousiometry/data/ousiometer/ousiometry_data_SPS.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
%% process settings
%%%%%%%%%%%%%%%%%%%%%%%%



if (~isfield(settings,'indexpair'))
    error('indexpair missing from settings');
else
    indexpair = settings.indexpair;
    dim1 = indexpair(1);
    dim2 = indexpair(2);
end

if (~isfield(settings,'indexpair_i'))
    error('indexpair_i missing from settings');
else
    indexpair_i = settings.indexpair_i;
end


if (~isfield(settings,'coords'))
    settings.coords = 'PDS';
else
    coordsindex = mapcoords(settings.coords);
end

if (~isfield(settings,'filetag'))
    error('need a tag set for file name');
else
    filetag = settings.filetag;
end


%% annotations through the middle
if (~isfield(settings,'Nannotations_horizontal'))
    
else
    Nannotations_horizontal = settings.Nannotations_horizontal;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% preliminary handling of counts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zipfcounts = col(zipfcounts);
totalzipfcounts = sum(zipfcounts,2);
zipfindices = find(totalzipfcounts > 0);

zipfwords = words(zipfindices);
word_ousiometric_avgs = ousiometry_data(coordsindex).avgs(:,zipfindices);
wordfreqs = totalzipfcounts(zipfindices);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% create ordered distributions for statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:3
    [~,indices] = sort(word_ousiometric_avgs(i,:),'ascend');
    sortedavgs(i,1:length(zipfwords)) = row(word_ousiometric_avgs(i,indices));
    freqs(i,1:length(zipfwords)) = row(wordfreqs(indices));
    probs(i,1:length(zipfwords)) = row(wordfreqs(indices)/sum(wordfreqs(indices)));
    truemedians(i) = sortedavgs(i,min(find(cumsum(probs(i,:)) >= 0.5)));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% figure building 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

more off;

%%% 

loadcolors;

%%% 


figure('visible','off');
set(gcf,'color','none');
tmpfigh = gcf;


clf;
figshape(1000,1000);
%% automatically create postscript whenever
%% figure is drawn
tmpfilename = 'figousiometer9001';
tmpcommand = sprintf('mkdir -p %s',tmpfilename);
system(tmpcommand);

tmpfilenoname = sprintf('%s/%s_%s_noname',tmpfilename,tmpfilename,filetag);
tmpfilesharename = sprintf('%s_',tmpfilename);

%% global switches

set(gcf,'Color','none');
set(gcf,'InvertHardCopy', 'off');

set(gcf,'DefaultAxesFontname','times');
set(gcf,'Renderer','Painters');

set(gcf,'DefaultAxesColor','none');
set(gcf,'DefaultLineMarkerSize',10);
% set(gcf,'DefaultLineMarkerEdgeColor','k');
set(gcf,'DefaultLineMarkerFaceColor','w');
set(gcf,'DefaultAxesLineWidth',0.5);

set(gcf,'PaperPositionMode','auto');

%% tmpsym = {'ok-','sk-','dk-','vk-','^k-','>k-','<k-','pk-','hk-'};
%% tmpsym = {'k-','r-','b-','m-','c-','g-','y-'};
%% tmpsym = {'k-','k-.','k:','k--','r-','r-.','r:','r--'};
%% tmplw = [ 1.5*ones(1,4), .5*ones(1,4)];


%% one simple plot
axes_positions(1).box = [.20 .20 .70 .70];

%% %% create an array of plots
%% 
%% xoffset = 0.05;
%% yoffset = 0.05; 
%% width = .30;
%% height = .30; 
%% xsep = 0.02;
%% ysep = 0.02;
%% numrows = 1; 
%% numcols = 3;
%% axes_positions = makeaxes_positions_grid(xoffset,...
%%                             yoffset,...
%%                             width,...
%%                             height,...
%%                             xsep,...
%%                             ysep,...
%%                             numrows,...
%%                             numcols);

%% %% create the same inset axis for each axis
%% 
%% 
%%  relative_xoffset = 0.10;
%%  relative_yoffset = 0.70;
%%  relative_width = 0.20;
%%  relative_height = 0.20;
%% 
%% inset_positions = makeaxes_inset_positions(...
%%    axes_positions,...
%%    relative_xoffset,...
%%    relative_yoffset,...
%%    relative_width,...
%%    relative_height)



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% histogram
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

binsize = 0.1/3;
xbinedges = [-1:binsize:1]';
ybinedges = [-1:binsize:1]';
edges{1} = xbinedges;
edges{2} = ybinedges;

Nx = (length(xbinedges) - 1)/2;
Ny = (length(ybinedges) - 1)/2;

%% underlying lexicon (not crucial but shape is used, residual code)

[binnedcounts,centers] = hist3(word_ousiometric_avgs(indexpair,:)',...
                         'edges',edges);

%% marginals for underlying lexicon

xbinned_counts = sum(binnedcounts,2);
ybinned_counts = sum(binnedcounts,1);

%% actual counts (may still be lexicon)

binned_zipfcounts = zeros(size(binnedcounts));
%% zipf distribution
for i=1:length(zipfindices)
    index_1 = Nx + 1 + floor(word_ousiometric_avgs(dim1,i) / binsize);
    index_2 = Ny + 1 + floor(word_ousiometric_avgs(dim2,i) / binsize);
    binned_zipfcounts(index_1,index_2) = ...
        binned_zipfcounts(index_1,index_2) + totalzipfcounts(zipfindices(i));
end

%% marginals

xbinned_zipfcounts = sum(binned_zipfcounts,2);
ybinned_zipfcounts = sum(binned_zipfcounts,1);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% main plot
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

axesnum = 1;
tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);




%% color map
if (isfield(settings,'colormaptype')) 
    if (strcmp(settings.colormaptype,'normalized'))
        overallmaxcounts = 100;
    end
elseif (~isfield(settings,'overallmaxcounts'))
    overallmaxcounts = 100;
    settings.colormaptype = 'normalized';
else 
    overallmaxcounts = settings.overallmaxcounts;
    settings.colormaptype = 'indexed';
end
%% use part of magma
heatmapcolors = flipud(magma(3*overallmaxcounts));

things.maxcolorindex_data = max(binned_zipfcounts(:));

if (strcmp(settings.colormaptype,'indexed'))
    for i=1:size(binned_zipfcounts,1)-1
        for j=1:size(binned_zipfcounts,2)-1
            if (binned_zipfcounts(i,j) > 0)
                tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                tmph = rectangle('position',tmppos);
                hold on;
                
                colorindex = binned_zipfcounts(i,j);
                if (colorindex > overallmaxcounts)
                    fprintf(1,'colorindex = %d, above %d\n', ...
                            colorindex, ...
                            overallmaxcounts);
                    colorindex = overallmaxcounts;
                end
                set(tmph,'facecolor',heatmapcolors(colorindex,:));
                set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                
                
            end
        end
    end
else
    for i=1:size(binned_zipfcounts,1)-1
        for j=1:size(binned_zipfcounts,2)-1
            if (binned_zipfcounts(i,j) > 0)
                tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                tmph = rectangle('position',tmppos);
                hold on;
                
                colorindex = 1+floor(0.999*overallmaxcounts*binned_zipfcounts(i,j) / max(binned_zipfcounts(:)));
                set(tmph,'facecolor',heatmapcolors(colorindex,:));
                set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                
                
            end
        end
    end
end


set(gca,'color',colors.lightergrey);
set(gca,'fontsize',18)
%% axis([]);
% xlim([-.3 1.4]);
% ylim([-.2 1.5]);

%% xlim([-.85 .85]);
%% ylim([-.85 .85]);

xlim([-1 1]);
ylim([-1 1]);

grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% color bar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

colorbarpos = [-.975 +0.65];

addcolorbar;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% marginals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

xbinned_zipfcounts = sum(binned_zipfcounts,2);
ybinned_zipfcounts = sum(binned_zipfcounts,1);

binsize = 0.1/3;

offset = 0.03;

%% maxmarginalbinnedcounts = 300;

set(gca,'clipping','off');

marginal_dist_height = 3;

for i=1:size(xbinned_zipfcounts,1)-1
    if (xbinned_zipfcounts(i) > 0)
        height = marginal_dist_height*binsize * xbinned_zipfcounts(i) / max(xbinned_zipfcounts);
        %%        tmppos = [xbinedges(i), 1+offset, binsize, binsize];
        %%        tmppos = [xbinedges(i), -1 + offset + 2*binsize - height/2, binsize, height];
        tmppos = [xbinedges(i), +1 + 0*offset, binsize, height];
        tmph = rectangle('position',tmppos);
        hold on;
        
        %%         colorindex = 1+round(maxmarginalcounts* ...
        %%                              xbinned_zipfcounts(i)/max(xbinned_zipfcounts));
        %%         set(tmph,'facecolor',heatmapcolors(colorindex,:));
        %%         set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));

        %% 
        %%        set(tmph,'edgecolor',colors.lightgreyish);
        
        if (xbinedges(i) >= 0)
            set(tmph,'facecolor',colors.grey);
        else
            set(tmph,'facecolor',colors.lightgrey);
        end
        set(tmph,'edgecolor',colors.darkgrey);
    
    end
end

for i=1:size(ybinned_zipfcounts,2)-1
    if (ybinned_zipfcounts(i) > 0)

        width = marginal_dist_height*binsize * ybinned_zipfcounts(i) / max(ybinned_zipfcounts);
        tmppos = [+1 + 0*offset, ybinedges(i), width, binsize];
        tmph = rectangle('position',tmppos);
        hold on;
        
        %%        colorindex = 1+round(maxmarginalcounts*ybinned_zipfcounts(i)/max(ybinned_zipfcounts));
        %%        set(tmph,'facecolor',heatmapcolors(colorindex,:));
        %%        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
        
        
        %%        set(tmph,'facecolor',colors.lightgreyer);
        %%        set(tmph,'edgecolor',colors.lightgreyish);
    
        if (ybinedges(i) >= 0)
            set(tmph,'facecolor',colors.grey);
        else
            set(tmph,'facecolor',colors.lightgrey);
        end
        set(tmph,'edgecolor',colors.darkgrey);

    end
end

%% add indicators for medians

xmedian = truemedians(dim1);
ymedian = truemedians(dim2);

tmph = plot(xmedian, +1 + 0.02,'v');
set(tmph,'markerfacecolor',colors.verydarkgrey);
set(tmph,'markeredgecolor',colors.verydarkgrey);
set(tmph,'markersize',10);

tmph = plot(+1 + 0.02,ymedian,'<');
set(tmph,'markerfacecolor',colors.verydarkgrey);
set(tmph,'markeredgecolor',colors.verydarkgrey);
set(tmph,'markersize',10);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmpXcoord = 0.0;
tmpYcoord = 1.17;
if (isfield(settings,'titleinsert'))
    settings.title = sprintf('$\\sim$ %s-%s ousiogram for %s $\\sim$',...
                             dimensions(dim1,coordsindex).namelc, ...
                             dimensions(dim2,coordsindex).namelc, ...
                             settings.titleinsert);
elseif (~isfield(settings,'title'))
    settings.title = sprintf('$\\sim$ ousiogram for %s-%s space $\\sim$',...
                             dimensions(dim1,coordsindex).namelc, ...
                             dimensions(dim2,coordsindex).namelc);
end
tmpstr = settings.title;

tmpcolor = colors.verydarkgrey;
tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
            'fontsize',26,...
            'units','data',...
            'horizontalalignment','center',...
            'verticalalignment','middle',...
            'color',tmpcolor, ...
            'interpreter','latex');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% lexicon size, total word count
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tmpXcoord = -0.96;
tmpYcoord = -0.98;
clear tmpstr;

if (isfield(settings,'Ntypes')) 
    tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
                        addcommas(settings.Ntypes));
else
    tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
                        addcommas(sum(totalzipfcounts>0)));
end

if (isfield(settings,'Ntokens')) 
    tmpstr{2} = sprintf('$N_{\\rm words}$ = %s', ...
                        addcommas(settings.Ntokens));
else
    tmpstr{2} = sprintf('$N_{\\rm words}$ = %s', ...
                        addcommas(sum(totalzipfcounts)));
end

tmpcolor = colors.darkergrey;
tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
            'fontsize',16,...
            'units','data',...
            'horizontalalignment','left',...
            'verticalalignment','bottom',...
            'color',tmpcolor, ...
            'interpreter','latex');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add optional svd fit: axes and ellipse for underlying lexicon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(settings,'showellipse') & strcmp(settings.showellipse,'yes')==1)

    Upair = ousiometry_data(coordsindex).svd_data(indexpair_i).Upair;
    sigmaspair = ousiometry_data(coordsindex).svd_data(indexpair_i).sigmaspair;

    slope = Upair(2,1)/Upair(1,1);
    angle = atan(slope);

    %% axes

    maxhalfwidth = 0.45;
    tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
    tmpr1 = sigmaspair(1)/sigmaspair(1);
    tmpx = tmpt*tmpr1*cos(angle);
    tmpy = tmpt*tmpr1*sin(angle);

    tmph = plot(tmpx,tmpy,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);


    slope = Upair(2,2)/Upair(1,2);
    angle = atan(slope);

    tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
    tmpr2 = sigmaspair(2)/sigmaspair(1);
    tmpx = tmpt*tmpr2*cos(angle);
    tmpy = tmpt*tmpr2*sin(angle);

    tmph = plot(tmpx,tmpy,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);
    
    %% add diagonals
    if (axesnum==7)
        maxhalfwidth = 0.45;
        tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
        tmpr1 = sigmaspair(1)/sigmaspair(1);
        tmpx = tmpt*tmpr1*cos(pi/4);
        tmpy = tmpt*tmpr1*sin(pi/4);

        tmph = plot(tmpx,tmpy,'k-');
        set(tmph,'linewidth',0.5);
        set(tmph,'color',colors.verydarkgrey);
        
        maxhalfwidth = 0.45;
        tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
        tmpr1 = sigmaspair(1)/sigmaspair(1);
        tmpx = tmpt*tmpr1*cos(pi/4);
        tmpy = tmpt*tmpr1*sin(-pi/4);

        tmph = plot(tmpx,tmpy,'k-');
        set(tmph,'linewidth',0.5);
        set(tmph,'color',colors.verydarkgrey);
        
    end

    %% ellipse

    tmprotation = [cos(angle) -sin(angle); sin(angle) cos(angle)];

    tmptheta = linspace(0,2*pi,100);
    tmpx = maxhalfwidth*tmpr2*cos(tmptheta);
    tmpy = maxhalfwidth*tmpr1*sin(tmptheta);

    tmpxrot = tmprotation(1,:) * [tmpx; tmpy];
    tmpyrot = tmprotation(2,:) * [tmpx; tmpy];

    tmph = plot(tmpxrot,tmpyrot,'k-');
    set(tmph,'linewidth',0.5);
    set(tmph,'color',colors.verydarkgrey);

    if ((coordsindex==3) & (indexpair_i==1))
                set(tmph,'linestyle','--');
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

annotatedwords = zeros(length(zipfwords),1);

tmpoffset = 0.02;

annotation_settings.binwidth = 1/20;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: medians on eight paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% annotate moving out from center
%% avoid some number of central words
%% 
%% default number of annotations from the edge is 7
%% 
%% todo: make number of words in each dimension adjustable
%% todo: prevent words from reprinting


if (isfield(settings,'do_not_annotate')) 
    annotatedtypeslist = settings.do_not_annotate;
else
    clear annotatedtypeslist;
    annotatedtypeslist{1} = 'woifjwoijfweof';
end

if (isfield(settings,'Nmedian_start')) 
    Nmedian_start = settings.Nmedian_start;
else
    Nmedian_start = 5;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: along each eight paths
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% todo
%% settings.annotation_angles

xvals = word_ousiometric_avgs(indexpair(1),:);
yvals = word_ousiometric_avgs(indexpair(2),:);

otherdim = setdiff([1:3],indexpair);

zvals = word_ousiometric_avgs(otherdim,:);

phivals = [0:45:315]*pi/180;

phi1 = 45;
textangles = [0, -phi1, -90, phi1, 0, -phi1, 90, phi1];

for i_phi = 1:length(phivals)
    phi = phivals(i_phi);

    Rotation = [
        cos(phi) -sin(phi);
        sin(phi) cos(phi)
               ];

    xyvals = [row(xvals); row(yvals)];
    xyvals_rot = Rotation*xyvals;
    
    xyzvals_rot = [xyvals_rot; row(zvals)];

    xvals_rot = xyvals_rot(1,:);
    yvals_rot = xyvals_rot(2,:);

    %% back to normal 2-d view

    ymin_rot = min(yvals_rot);
    ymax_rot = max(yvals_rot);

    %% bin along the positive vertical axis
    nmin = 0; %% center
    nmax = ceil(ymax_rot/annotation_settings.binwidth);

    binedges = (nmin:1:nmax)'*annotation_settings.binwidth;
    bincenters = binedges(1:end-1) + annotation_settings.binwidth/2;


    %% todo: add more options here
    if (isfield(settings,'annotation_type')) 
        if(strcmp(settings.annotation_type,'outer'))
            startindex = Nmedian_start;
        else
            error('Annotation type must be one of: outer (oops)');
        end
    else
        %% annotate center column fully
        if (i_phi == 1)
            startindex = 1;
        elseif (i_phi == 5)
            startindex = 1;
        elseif (mod(i_phi,2)==1)
            startindex = Nmedian_start;
        else 
            startindex = Nmedian_start + 1;
        end
    end
    
    for bin_i = startindex:length(binedges)-1
        
        binindices = find((yvals_rot >= binedges(bin_i)) & ...
                          (yvals_rot < binedges(bin_i + 1)));

        %% tight to center line
        %% [minval,index] = min(sqrt(sum(xyzvals_rot([1, 3],binindices).^2)));
        
        %% ignoring third dimension
        [minval,index] = min(abs(xyzvals_rot(1,binindices)));
        
        if ((length(index)>0) & (minval < 3*annotation_settings.binwidth))
            word = zipfwords{binindices(index)};

            if (~strcmp(annotatedtypeslist,word)) %% unless already annotated

                annotatedtypeslist{end+1} = word;
                
                tmpXcoord_rot = 0;
                tmpYcoord_rot = binedges(bin_i) + annotation_settings.binwidth/2;

                tmpXYcoords = inv(Rotation)*[tmpXcoord_rot; tmpYcoord_rot];
                tmpXcoord = tmpXYcoords(1);
                tmpYcoord = tmpXYcoords(2);
                
                tmpstr = sprintf('%s',word);

                if (mod(bin_i,2)==1)
                    tmpcolor = colors.darkergrey;
                else
                    tmpcolor = colors.verydarkgrey;
                end

                tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                            'fontsize',18,...
                            'units','data',...
                            'horizontalalignment','center',...
                            'color',tmpcolor, ...
                            'rotation',textangles(i_phi), ...
                            'verticalalignment','middle',...
                            'interpreter','latex');
            end
        end
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% annotations: convex hull
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

annotation_settings.binwidth = 1/20;
annotation_settings.fontsize = 20;
annotation_settings.breakangle = 3/4 * pi;
annotation_settings.textoffset = 0.05;

%% set in ousiometry settings
annotation_settings.excludedtypes = annotatedtypeslist;

annotatedtypeslist = annotate_histogram_convexhull_types001( ...
    word_ousiometric_avgs(dim1,:)', ...
    word_ousiometric_avgs(dim2,:)', ...
    zipfwords, ...
    annotation_settings);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% categories 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% trickiness: matrices are flips of how we want them plotted
%% clockwise, starting from the top of the clock

k=0;
k=k+1;
categories{k} = dimensions(dim2,coordsindex).positive1word;
k=k+1;
categories{k} = sprintf('%s-%s', ...
                        dimensions(dim2,coordsindex).positive1word, ...
                        dimensions(dim1,coordsindex).positive1word);
k=k+1;
categories{k} = dimensions(dim1,coordsindex).positive1word;
k=k+1;
categories{k} = sprintf('%s-%s', ...
                        dimensions(dim2,coordsindex).negative1word, ...
                        dimensions(dim1,coordsindex).positive1word);
k=k+1;
categories{k} = dimensions(dim2,coordsindex).negative1word;
k=k+1;
categories{k} = sprintf('%s-%s', ...
                        dimensions(dim1,coordsindex).negative1word, ...
                        dimensions(dim2,coordsindex).negative1word);
k=k+1;
categories{k} = dimensions(dim1,coordsindex).negative1word;
k=k+1;
categories{k} = sprintf('%s-%s', ...
                        dimensions(dim1,coordsindex).negative1word, ...
                        dimensions(dim2,coordsindex).positive1word);

phivals = [0:45:315]*pi/180;

phi1 = 45;
textangles = [0, -phi1, -90, phi1, 0, -phi1, 90, phi1];

widths = std(word_ousiometric_avgs,[],2);

for i = 1:length(phivals)

%%     ellipticity = 1; %% 0.925;
%%     
%%     tmpXcoord = 0.95*sin(phivals(i));
%%     tmpYcoord = ellipticity*0.95*cos(phivals(i));

    if (mod(i,2)==1)
        tmpXcoord = 0.950*sin(phivals(i));
        tmpYcoord = 0.950*cos(phivals(i));
    else
        tmpXcoord = 1.000*sin(phivals(i));
        tmpYcoord = 1.000*cos(phivals(i));
    end
    
    
    clear tmpstr;
    tmpstr = sprintf('{%s}', ...
                     categories{i});
    
    tmpcolor = colors.superdarkgrey;

    tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                'fontsize',24,...
                'units','data',...
                'horizontalalignment','center',...
                'color',tmpcolor, ...
                'rotation',textangles(i), ...
                'verticalalignment','middle',...
                'interpreter','latex');

    
    %% arrows
    
%%     ellipticity = 1; %% 0.85;
%%     tmpXcoord = 0.875*sin(phivals(i));
%%     tmpYcoord = ellipticity*0.875*cos(phivals(i));

    if (mod(i,2)==1)
        tmpXcoord = 0.875*sin(phivals(i));
        tmpYcoord = 0.875*cos(phivals(i));
    else
        tmpXcoord = 0.925*sin(phivals(i));
        tmpYcoord = 0.925*cos(phivals(i));
    end
    
    
    
    if ((i<=3) | (i>=7))
        tmpstr = sprintf('${\\uparrow}$');
    else
        tmpstr = sprintf('${\\downarrow}$');
    end
    
    tmpcolor = colors.superdarkgrey;

    tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                'fontsize',24,...
                'units','data',...
                'horizontalalignment','center',...
                'color',tmpcolor, ...
                'rotation',textangles(i), ...
                'verticalalignment','middle',...
                'interpreter','latex');

end

%% change axis line width (default is 0.5)
%% set(tmpaxes(axesnum),'linewidth',2)

%% fix up tickmarks
set(gca,'xtick',[-1:.1:1]);
%% set(gca,'xticklabel',{'','',''})
set(gca,'ytick',[-1:.1:1]);
%% set(gca,'yticklabel',{'','',''})

%% the following will usually not be printed 
%% in good copy for papers
%% (except for legend without labels)

%% remove a plot from the legend
%% set(get(get(tmph,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

%% %% legend

%% tmplh = legend('stuff',...);
%% tmplh = legend('','','');
%% 
%% tmplh.Interpreter = 'latex';
%% set(tmplh,'position',get(tmplh,'position')-[x y 0 0])
%% %% change font
%% tmplh_obj = findobj(tmplh,'type','text');
%% set(tmplh_obj,'FontSize',20);
%% %% remove box:
%% legend boxoff

%% use latex interpreter for text, sans serif

clear tmpstr;

tmpstr = sprintf('$\\leftarrow$ %s \\quad %s $%s$ \\quad %s $\\rightarrow$', ...
                 dimensions(dim1,coordsindex).negative,...
                 dimensions(dim1,coordsindex).namelc,...
                 dimensions(dim1,coordsindex).notation,...
                 dimensions(dim1,coordsindex).positive);


tmpxlab=xlabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','top',...
               'interpreter','latex');



tmpstr = sprintf('$\\leftarrow$ %s  %s $%s$  %s $\\rightarrow$', ...
                 dimensions(dim2,coordsindex).negative,...
                 dimensions(dim2,coordsindex).namelc,...
                 dimensions(dim2,coordsindex).notation,...
                 dimensions(dim2,coordsindex).positive);


tmpylab=ylabel(tmpstr,...
               'fontsize',24,...
               'verticalalignment','bottom',...
               'interpreter','latex');

%% set(tmpxlab,'position',get(tmpxlab,'position') - [0 .1 0]);
%% set(tmpylab,'position',get(tmpylab,'position') - [.1 0 0]);

%% set 'units' to 'data' for placement based on data points
%% set 'units' to 'normalized' for relative placement within axes
%% tmpXcoord = ;
%% tmpYcoord = ;
%% tmpstr = sprintf(' ');
%% or
%% tmpstr{1} = sprintf(' ');
%% tmpstr{2} = sprintf(' ');
%%
%% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%     'fontsize',24,...
%%     'units','normalized',...
%%     'horizontalalignment','left',...
%%     'verticalalignment','top',...
%%     'interpreter','latex');

%% label (A, B, ...)
%% tmplabelh = addlabel4(' A ',0.02,0.9,20);
%% tmplabelh = addlabel5(loop_i,0.02,0.9,20);
%% or:
%% tmplabelXcoord= 0.015;
%% tmplabelYcoord= 0.88;
%% tmplabelbgcolor = 0.85;
%% tmph = text(tmplabelXcoord,tmplabelYcoord,...
%%    ' A ',...
%%    'fontsize',24,
%%         'units','normalized');
%%    set(tmph,'backgroundcolor',tmplabelbgcolor*[1 1 1]);
%%    set(tmph,'edgecolor',[0 0 0]);
%%    set(tmph,'linestyle','-');
%%    set(tmph,'linewidth',1);
%%    set(tmph,'margin',1);

%% rarely used (text command is better)
%% title(' ','fontsize',24,'interpreter','latex')
%% 'horizontalalignment','left');
%% tmpxl = xlabel('','fontsize',24,'verticalalignment','top');
%% set(tmpxl,'position',get(tmpxl,'position') - [ 0 .1 0]);
%% tmpyl = ylabel('','fontsize',24,'verticalalignment','bottom');
%% set(tmpyl,'position',get(tmpyl,'position') - [ 0.1 0 0]);
%% title('','fontsize',24)



%% %% tmph = plot(0,0,'ko');
%% %% set(tmph,'markerfacecolor',colors.darkgrey);
%% %% set(tmph,'markeredgecolor',colors.verydarkgrey);
%% 
%% clear tmpstr;
%% tmpstr{1} = sprintf('$%s \\sim %4.2f %s$', ...
%%                     dimensions_old(3).notation,...
%%                     slope,...
%%                     dimensions_old(2).notation);
%% tmpstr{2} = sprintf('$r \\simeq %4.2f$', ...
%%                     corrsVAD(2,3));
%% 
%% tmpXcoord = 0.20;
%% tmpYcoord = 0;
%% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%             'fontsize',20,...
%%             'units','data',...
%%             'horizontalalignment','center',...
%%             'color',tmpcolor, ...
%%             'rotation',0, ...
%%             'verticalalignment','middle',...
%%             'interpreter','latex');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% automatic creation of image file (pdf, postscript, png, ...)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(settings,'imageformat'))
    if (isfield(settings.imageformat,'open'))
        imageformat.open = settings.imageformat.open;
    else
        imageformat.open = 'yes';
    end
else
    imageformat.open = 'yes';
end

%% for pdf, without name/date
imageformat.type = 'pdf';
imageformat.dpi = 600;
imageformat.deleteps = 'yes';
%% imageformat.open = 'yes';
imageformat.copylink = 'no';

print_universal(tmpfilenoname,imageformat);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %% png with name/date
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% axesnum = 1;
%% axes(tmpaxes(axesnum));
%% %% tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);
%% 
%% clear tmpstr;
%% tmpstr = sprintf(' otcompstorylab');
%% %% dates covered: %s to %s', ... 
%% %%                 datestr(startdatenum,'yyyy/mm/dd'), ...
%% %%                 datestr(datenum2,'yyyy/mm/dd'));
%% 
%% tmpXcoord = 0.00;
%% tmpYcoord = 0.00;
%% 
%% text(tmpXcoord,tmpYcoord,tmpstr,...
%%      'fontsize',16,...
%%      'units','normalized',...
%%      'horizontalalignment','left',...
%%      'verticalalignment','bottom',...
%%      'rotation',0,...
%%      'color',0.7*[1 1 1],...
%%      'interpreter','latex')
%% 
%% print_universal(tmpfiletwittername,imageformat);
%% 
%% tmpcommand = sprintf('convert -background white -flatten -geometry 1600x -density 600 %s.pdf %s.png', ...
%%                      tmpfilesharename, ...
%%                      tmpfilesharename);
%% system(tmpcommand);
%% 
%% tmpcommand = sprintf('permanentshare-uvm.pl %s.png', ...
%%                      tmpfiletwittername);
%% system(tmpcommand);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% archivify (0 = off, non-zero = on)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

archiveswitch = 0;
figarchivify(tmpfilenoname,archiveswitch);


%% prevent hidden figure clutter/bloat
%% may need to switch this off for some test
close(tmpfigh);

%% clean up tmp* files
clear tmp*

more on;

