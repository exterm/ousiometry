function things = figousiometer9300(zipfcounts,settings)
%% 
%% things = figousiometer9300(zipfcounts,settings)
%% 
%% - creates a 3x3 tableau of ousiograms
%% - built out from figousiometer9000
%% 
%% set zipfcounts to all ones for a lexicon presentation
%% 
%% things (minor) = structure for outputs of possible use

load_ousiometry_settings;

load ousiometry_data.mat;
%% load('~/matlab/ousiometry/data/ousiometer/ousiometry_data.mat');


if (isfield(settings,'switchorder'))
    if (strcmp(settings.switchorder,'yes'))
        indexpairs{1} = [1 2];
        indexpairs{2} = [2 3];
        indexpairs{3} = [3 1];

        for coordsindex = 1:3
            tmp = ousiometry_data(coordsindex).svd_data(2).Upair;
            ousiometry_data(coordsindex).svd_data(2).Upair = ...
                ousiometry_data(coordsindex).svd_data(3).Upair;
            ousiometry_data(coordsindex).svd_data(3).Upair = tmp;
            
            tmp = ousiometry_data(coordsindex).svd_data(2).sigmaspair;
            ousiometry_data(coordsindex).svd_data(2).sigmaspair = ...
                ousiometry_data(coordsindex).svd_data(3).sigmaspair;
            ousiometry_data(coordsindex).svd_data(3).sigmaspair = tmp;
        
            ousiometry_data(coordsindex).svd_data(3).Upair = ...
                ousiometry_data(coordsindex).svd_data(3).Upair([2 1],:);
        end
    end
end
        

coordslist = {'VAD','GES','PDS'};
coordslistlabels = {'V-A-D','G-E-S','P-D-S'};
coordsfull = {'Valence-Arousal-Dominance',...
             'Goodness-Energy-Structure',...
             'Power-Danger-Structure'};


%%%%%%%%%%%%%%%%%%%%%%%%
%% process settings
%%%%%%%%%%%%%%%%%%%%%%%%


if (~isfield(settings,'filetag'))
    error('need a tag set for file name');
else
    filetag = settings.filetag;
end


%% annotations through the middle
if (~isfield(settings,'Nannotations_horizontal'))
    
else
    Nannotations_horizontal = settings.Nannotations_horizontal;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% preliminary handling of counts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zipfcounts = col(zipfcounts);
totalzipfcounts = sum(zipfcounts,2);
zipfindices = find(totalzipfcounts > 0);
zipfwords = words(zipfindices);
wordfreqs = totalzipfcounts(zipfindices);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% figure building 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

more off;

%%% 

loadcolors;

%%% 


figure('visible','off');
set(gcf,'color','none');
tmpfigh = gcf;


clf;
figshape(1500,1500);
%% automatically create postscript whenever
%% figure is drawn
tmpfilename = 'figousiometer9300';
tmpcommand = sprintf('mkdir -p %s',tmpfilename);
system(tmpcommand);

tmpfilenoname = sprintf('%s/%s_%s_noname',tmpfilename,tmpfilename,filetag);
tmpfilesharename = sprintf('%s_',tmpfilename);

%% global switches

set(gcf,'Color','none');
set(gcf,'InvertHardCopy', 'off');

set(gcf,'DefaultAxesFontname','times');
set(gcf,'Renderer','Painters');

set(gcf,'DefaultAxesColor','none');
set(gcf,'DefaultLineMarkerSize',10);
% set(gcf,'DefaultLineMarkerEdgeColor','k');
set(gcf,'DefaultLineMarkerFaceColor','w');
set(gcf,'DefaultAxesLineWidth',0.5);

set(gcf,'PaperPositionMode','auto');

%% tmpsym = {'ok-','sk-','dk-','vk-','^k-','>k-','<k-','pk-','hk-'};
%% tmpsym = {'k-','r-','b-','m-','c-','g-','y-'};
%% tmpsym = {'k-','k-.','k:','k--','r-','r-.','r:','r--'};
%% tmplw = [ 1.5*ones(1,4), .5*ones(1,4)];



%% create an array of plots

xoffset = 0.10;
yoffset = 0.05; 
width = .25;
height = .25; 
xsep = 0.05;
ysep = 0.05;
numrows = 3; 
numcols = 3;
axes_positions = makeaxes_positions_grid(xoffset,...
                            yoffset,...
                            width,...
                            height,...
                            xsep,...
                            ysep,...
                            numrows,...
                            numcols);
axes_positions = axes_positions';

%% for background glue
delta = 0.02;
xoffset = 0.05;
yoffset = 0.05 + delta; 
width = .75;
height = .25 - 2*delta; 
xsep = 0.05;
ysep = 0.05 + 2*delta;
numrows = 3; 
numcols = 1;
axes_positions_bg = makeaxes_positions_grid(xoffset,...
                            yoffset,...
                            width,...
                            height,...
                            xsep,...
                            ysep,...
                            numrows,...
                            numcols);



%% %% create the same inset axis for each axis
%% 
%% 
%%  relative_xoffset = 0.10;
%%  relative_yoffset = 0.70;
%%  relative_width = 0.20;
%%  relative_height = 0.20;
%% 
%% inset_positions = makeaxes_inset_positions(...
%%    axes_positions,...
%%    relative_xoffset,...
%%    relative_yoffset,...
%%    relative_width,...
%%    relative_height)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% add background connection
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

axesnum = 0;
for loop_i = 1:length(axes_positions_bg)
    axesnum = axesnum + 1;
    tmpaxes(axesnum) = axes('position',axes_positions_bg(loop_i).box);

    tmpx = [0 1 1 0];
    tmpy = [0 0 1 1];
    tmph = fill(tmpx,tmpy,0.9*[1 1 1]);
    set(tmph,'edgecolor',0.9*[1 1 1]);
    hold on;
    
    set(gca,'visible','off');
    set(gca,'clipping','on');
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% make panels
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



axesnum = 0;
for loop_j=1:length(coordslist)
    coords = coordslist{loop_j};
    coordsindex = mapcoords(coords);
    
    for indexpair_i = 1:length(indexpairs)

        indexpair = indexpairs{indexpair_i};
        dim1 = indexpair(1);
        dim2 = indexpair(2);

        word_ousiometric_avgs = ousiometry_data(coordsindex).avgs(:,zipfindices);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% create ordered distributions for statistics
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        for i=1:3
            [~,indices] = sort(word_ousiometric_avgs(i,:),'ascend');
            sortedavgs(i,1:length(zipfwords)) = row(word_ousiometric_avgs(i,indices));
            freqs(i,1:length(zipfwords)) = row(wordfreqs(indices));
            probs(i,1:length(zipfwords)) = row(wordfreqs(indices)/sum(wordfreqs(indices)));
            truemedians(i) = sortedavgs(i,min(find(cumsum(probs(i,:)) >= 0.5)));
        end





        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% histogram
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        binsize = 0.1/2;
        xbinedges = [-1:binsize:1]';
        ybinedges = [-1:binsize:1]';
        edges{1} = xbinedges;
        edges{2} = ybinedges;

        Nx = (length(xbinedges) - 1)/2;
        Ny = (length(ybinedges) - 1)/2;

        %% underlying lexicon (not crucial but shape is used, residual code)

        [binnedcounts,centers] = hist3(word_ousiometric_avgs(indexpair,:)',...
                                       'edges',edges);

        %% marginals for underlying lexicon

        xbinned_counts = sum(binnedcounts,2);
        ybinned_counts = sum(binnedcounts,1);

        %% actual counts (may still be lexicon)

        binned_zipfcounts = zeros(size(binnedcounts));
        %% zipf distribution
        for i=1:length(zipfindices)
            index_1 = Nx + 1 + floor(word_ousiometric_avgs(dim1,i) / binsize);
            index_2 = Ny + 1 + floor(word_ousiometric_avgs(dim2,i) / binsize);
            binned_zipfcounts(index_1,index_2) = ...
                binned_zipfcounts(index_1,index_2) + totalzipfcounts(zipfindices(i));
        end

        %% marginals

        xbinned_zipfcounts = sum(binned_zipfcounts,2);
        ybinned_zipfcounts = sum(binned_zipfcounts,1);




        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% main plot
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        axesnum = axesnum + 1;
        tmpaxes(axesnum) = axes('position',axes_positions(loop_j,indexpair_i).box);

        %% color map
        if (isfield(settings,'colormaptype')) 
            if (strcmp(settings.colormaptype,'normalized'))
                overallmaxcounts = 100;
            end
        elseif (~isfield(settings,'overallmaxcounts'))
            overallmaxcounts = 100;
            settings.colormaptype = 'normalized';
        else 
            overallmaxcounts = settings.overallmaxcounts;
            settings.colormaptype = 'indexed';
        end
        %% use part of magma
        heatmapcolors = flipud(magma(3*overallmaxcounts));

        things.maxcolorindex_data = max(binned_zipfcounts(:));

        if (strcmp(settings.colormaptype,'indexed'))
            for i=1:size(binned_zipfcounts,1)-1
                for j=1:size(binned_zipfcounts,2)-1
                    if (binned_zipfcounts(i,j) > 0)
                        tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                        tmph = rectangle('position',tmppos);
                        hold on;
                        
                        colorindex = binned_zipfcounts(i,j);
                        if (colorindex > overallmaxcounts)
                            fprintf(1,'colorindex = %d, above %d\n', ...
                                    colorindex, ...
                                    overallmaxcounts);
                            colorindex = overallmaxcounts;
                        end
                        set(tmph,'facecolor',heatmapcolors(colorindex,:));
                        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                        
                        
                    end
                end
            end
        else
            for i=1:size(binned_zipfcounts,1)-1
                for j=1:size(binned_zipfcounts,2)-1
                    if (binned_zipfcounts(i,j) > 0)
                        tmppos = [xbinedges(i) ybinedges(j) binsize binsize];
                        tmph = rectangle('position',tmppos);
                        hold on;
                        
                        colorindex = 1+floor(0.999*overallmaxcounts*binned_zipfcounts(i,j) / max(binned_zipfcounts(:)));
                        set(tmph,'facecolor',heatmapcolors(colorindex,:));
                        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                        
                        
                    end
                end
            end
        end


        if (axesnum >= 1)
            set(gca,'color',colors.lightergrey);
        else
            set(gca,'color',0.999*[1 1 1]);
        end
        
        set(gca,'fontsize',18)

        %% axis([]);
        % xlim([-.3 1.4]);
        % ylim([-.2 1.5]);

        %% xlim([-.85 .85]);
        %% ylim([-.85 .85]);

        xlim([-1 1]);
        ylim([-1 1]);

        grid on;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% color bar
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (axesnum == 7)
            colorbarpos = [-.975 +0.65];
            
            addcolorbar;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% marginals
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        xbinned_zipfcounts = sum(binned_zipfcounts,2);
        ybinned_zipfcounts = sum(binned_zipfcounts,1);

        binsize = 0.1/2;

        offset = 0.03;

        %% maxmarginalbinnedcounts = 300;

        set(gca,'clipping','off');

        for i=1:size(xbinned_zipfcounts,1)-1
            if (xbinned_zipfcounts(i) > 0)
                height = 4*binsize * xbinned_zipfcounts(i) / max(xbinned_zipfcounts);
                %%        tmppos = [xbinedges(i), 1+offset, binsize, binsize];
                %%        tmppos = [xbinedges(i), -1 + offset + 2*binsize - height/2, binsize, height];
                tmppos = [xbinedges(i), -1 + 0*offset, binsize, height];
                tmph = rectangle('position',tmppos);
                hold on;
                
                %%         colorindex = 1+round(maxmarginalcounts* ...
                %%                              xbinned_zipfcounts(i)/max(xbinned_zipfcounts));
                %%         set(tmph,'facecolor',heatmapcolors(colorindex,:));
                %%         set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));

                %%                set(tmph,'facecolor',colors.grey);
                %%                set(tmph,'edgecolor',colors.darkgrey);
                
                if (xbinedges(i) >= 0)
                    set(tmph,'facecolor',colors.grey);
                else
                    set(tmph,'facecolor',colors.lightgrey);
                end
                set(tmph,'edgecolor',colors.darkgrey);

            end
        end

        for i=1:size(ybinned_zipfcounts,2)-1
            if (ybinned_zipfcounts(i) > 0)

                width = 4*binsize * ybinned_zipfcounts(i) / max(ybinned_zipfcounts);
                tmppos = [-1 + 0*offset, ybinedges(i), width, binsize];
                tmph = rectangle('position',tmppos);
                hold on;
                
                %%        colorindex = 1+round(maxmarginalcounts*ybinned_zipfcounts(i)/max(ybinned_zipfcounts));
                %%        set(tmph,'facecolor',heatmapcolors(colorindex,:));
                %%        set(tmph,'edgecolor',0.7*heatmapcolors(colorindex,:));
                
                
                %%                set(tmph,'facecolor',colors.grey);
                %%                set(tmph,'edgecolor',colors.darkgrey);

                if (ybinedges(i) >= 0)
                    set(tmph,'facecolor',colors.grey);
                else
                    set(tmph,'facecolor',colors.lightgrey);
                end
                set(tmph,'edgecolor',colors.darkgrey);

            end
        end

        %% add indicators for medians

        xmedian = truemedians(dim1);
        ymedian = truemedians(dim2);

        tmph = plot(xmedian,-1 + 0.037,'v');
        set(tmph,'markerfacecolor',colors.verydarkgrey);
        set(tmph,'markeredgecolor',colors.verydarkgrey);
        set(tmph,'markersize',10);

        tmph = plot(-1 + 0.037 ,ymedian,'<');
        set(tmph,'markerfacecolor',colors.verydarkgrey);
        set(tmph,'markeredgecolor',colors.verydarkgrey);
        set(tmph,'markersize',10);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% title
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (axesnum==2)
            tmpXcoord = 0.0;
            tmpYcoord = 1.2;
            if (isfield(settings,'titleinsert'))
                settings.title = sprintf('Ousiograms for %s in the VAD, GES, and PDS frameworks:',...
                                         settings.titleinsert);
            end
            tmpstr = settings.title;
            
            tmpcolor = colors.verydarkgrey;
            tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                        'fontsize',28,...
                        'units','data',...
                        'horizontalalignment','center',...
                        'verticalalignment','middle',...
                        'color',tmpcolor, ...
                        'interpreter','latex');
        end

%%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%         %% lexicon size, total word count
%%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%%         tmpXcoord = -0.96;
%%         tmpYcoord = -0.98;
%%         clear tmpstr;
%% 
%%         if (isfield(settings,'Ntypes')) 
%%             tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
%%                                 addcommas(settings.Ntypes));
%%         else
%%             tmpstr{1} = sprintf('$N_{\\rm lexicon}$ = %s', ...
%%                                 addcommas(sum(totalzipfcounts>0)));
%%         end
%% 
%%         if (isfield(settings,'Ntokens')) 
%%             tmpstr{2} = sprintf('$N_{\\rm words}$ = %s', ...
%%                                 addcommas(settings.Ntokens));
%%         else
%%             tmpstr{2} = sprintf('$N_{\\rm words}$ = %s', ...
%%                                 addcommas(sum(totalzipfcounts)));
%%         end
%% 
%%         tmpcolor = colors.darkgrey;
%%         tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
%%                     'fontsize',16,...
%%                     'units','data',...
%%                     'horizontalalignment','left',...
%%                     'verticalalignment','bottom',...
%%                     'color',tmpcolor, ...
%%                     'interpreter','latex');

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% add optional svd fit: axes and ellipse for underlying lexicon
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        if (isfield(settings,'showellipse') & strcmp(settings.showellipse,'yes')==1)

            Upair = ousiometry_data(coordsindex).svd_data(indexpair_i).Upair;
            sigmaspair = ousiometry_data(coordsindex).svd_data(indexpair_i).sigmaspair;

            slope = Upair(2,1)/Upair(1,1);
            angle = atan(slope);
            
            if (axesnum==77)
                
                Upair = ousiometry_data(2).svd_data(indexpair_i).Upair;
                sigmaspair = ousiometry_data(2).svd_data(indexpair_i).sigmaspair;

                angle = -pi/4;
                
            end

            %% axes

            maxhalfwidth = 0.45;
            tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
            tmpr1 = sigmaspair(1)/sigmaspair(1);
            tmpx = tmpt*tmpr1*cos(angle);
            tmpy = tmpt*tmpr1*sin(angle);

            tmph = plot(tmpx,tmpy,'k-');
            set(tmph,'linewidth',0.5);
            set(tmph,'color',colors.verydarkgrey);


            slope = Upair(2,2)/Upair(1,2);
            angle = atan(slope);
            
            if (axesnum==77)
                angle = pi/4;
            end

            tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
            tmpr2 = sigmaspair(2)/sigmaspair(1);
            tmpx = tmpt*tmpr2*cos(angle);
            tmpy = tmpt*tmpr2*sin(angle);

            tmph = plot(tmpx,tmpy,'k-');
            set(tmph,'linewidth',0.5);
            set(tmph,'color',colors.verydarkgrey);

            %% add diagonals
            if (axesnum==77)
                maxhalfwidth = 0.45;
                tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
                tmpr1 = sigmaspair(1)/sigmaspair(1);
                tmpx = tmpt*tmpr1*cos(pi/4);
                tmpy = tmpt*tmpr1*sin(pi/4);

                tmph = plot(tmpx,tmpy,'k-');
                set(tmph,'linewidth',0.5);
                set(tmph,'color',colors.verydarkgrey);
                
                maxhalfwidth = 0.45;
                tmpt = linspace(-maxhalfwidth,maxhalfwidth,100)';
                tmpr1 = sigmaspair(1)/sigmaspair(1);
                tmpx = tmpt*tmpr1*cos(pi/4);
                tmpy = tmpt*tmpr1*sin(-pi/4);

                tmph = plot(tmpx,tmpy,'k-');
                set(tmph,'linewidth',0.5);
                set(tmph,'color',colors.verydarkgrey);
                
            end

            %% ellipse

            tmprotation = [cos(angle) -sin(angle); sin(angle) cos(angle)];

            tmptheta = linspace(0,2*pi,100);
            tmpx = maxhalfwidth*tmpr2*cos(tmptheta);
            tmpy = maxhalfwidth*tmpr1*sin(tmptheta);

            tmpxrot = tmprotation(1,:) * [tmpx; tmpy];
            tmpyrot = tmprotation(2,:) * [tmpx; tmpy];

            tmph = plot(tmpxrot,tmpyrot,'k-');
            set(tmph,'linewidth',0.5);
            set(tmph,'color',colors.verydarkgrey);
            
            if (axesnum==7)
                set(tmph,'linestyle','--');
            end
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% annotations
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        annotatedwords = zeros(length(zipfwords),1);

        tmpoffset = 0.02;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% annotations: convex hull
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        annotation_settings.binwidth = 1/10;
        annotation_settings.fontsize = 20;
        annotation_settings.breakangle = 3/4 * pi;
        annotation_settings.textoffset = 0.05;

        %% set in ousiometry settings
        %% annotation_settings.excludedtypes = {''}
        %% {'peaceful','peacetime','blessing','idle'};

        annotate_histogram_convexhull_types001( ...
            word_ousiometric_avgs(dim1,:)', ...
            word_ousiometric_avgs(dim2,:)', ...
            zipfwords, ...
            annotation_settings);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% annotations: medians down main column
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        idim = indexpair(2);
        otherdims = setdiff([1:3],idim);

        N = floor((max(word_ousiometric_avgs(idim,:)) - min(word_ousiometric_avgs(idim,:)))/annotation_settings.binwidth);

        binedges = linspace(min(word_ousiometric_avgs(idim,:)),...
                            max(word_ousiometric_avgs(idim,:)),...
                            N + 1);

        Nmedianannotations = 0;

        for i=[1:Nmedianannotations, N-Nmedianannotations+1:N]
            indices = find(((word_ousiometric_avgs(idim,:))>=binedges(i)) & ...
                           ((word_ousiometric_avgs(idim,:))<binedges(i+1)));
            [~,index] = min(sum(word_ousiometric_avgs(indexpair,indices).^2));
            if (length(index)>0)
                indices(index);
                word = zipfwords{indices(index)};
                
                tmpXcoord = 0;
                tmpYcoord = binedges(i) + annotation_settings.binwidth/2;
                tmpstr = sprintf('%s',word);

                if (mod(i,2)==1)
                    tmpcolor = colors.darkergrey;
                else
                    tmpcolor = colors.verydarkgrey;
                end

                tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                            'fontsize',18,...
                            'units','data',...
                            'horizontalalignment','center',...
                            'color',tmpcolor, ...
                            'verticalalignment','middle',...
                            'interpreter','latex');

            end
            
        end

        %% trickiness: matrices are flips of how we want them plotted
        %% clockwise, starting from the top of the clock

        k=0;
        k=k+1;
        categories{k} = dimensions(dim2,coordsindex).positive1word;
        k=k+1;
        categories{k} = sprintf('%s-%s', ...
                                dimensions(dim2,coordsindex).positive1word, ...
                                dimensions(dim1,coordsindex).positive1word);
        k=k+1;
        categories{k} = dimensions(dim1,coordsindex).positive1word;
        k=k+1;
        categories{k} = sprintf('%s-%s', ...
                                dimensions(dim2,coordsindex).negative1word, ...
                                dimensions(dim1,coordsindex).positive1word);
        k=k+1;
        categories{k} = dimensions(dim2,coordsindex).negative1word;
        k=k+1;
        categories{k} = sprintf('%s-%s', ...
                                dimensions(dim1,coordsindex).negative1word, ...
                                dimensions(dim2,coordsindex).negative1word);
        k=k+1;
        categories{k} = dimensions(dim1,coordsindex).negative1word;
        k=k+1;
        categories{k} = sprintf('%s-%s', ...
                                dimensions(dim1,coordsindex).negative1word, ...
                                dimensions(dim2,coordsindex).positive1word);


        %% categories 

        phivals = [0:45:315]*pi/180;

        phi1 = 45;
        textangles = [0, -phi1, -90, phi1, 0, -phi1, 90, phi1];

        widths = std(word_ousiometric_avgs,[],2);

        for i = 1:0 %% length(phivals)

            ellipticity = 0.925;
            
            tmpXcoord = 0.95*sin(phivals(i));
            tmpYcoord = ellipticity*0.95*cos(phivals(i));
            
            clear tmpstr;
            tmpstr = categories{i};
            
            tmpcolor = colors.verydarkgrey;

            tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                        'fontsize',24,...
                        'units','data',...
                        'horizontalalignment','center',...
                        'color',tmpcolor, ...
                        'rotation',textangles(i), ...
                        'verticalalignment','middle',...
                        'interpreter','latex');

            
            %% arrows
            
            if (mod(i,2)==1)
                tmpXcoord = 0.875*sin(phivals(i));
                tmpYcoord = 0.875*cos(phivals(i));
            else
                tmpXcoord = 0.925*sin(phivals(i));
                tmpYcoord = 0.925*cos(phivals(i));
            end
            
            ellipticity = 0.85;
            tmpXcoord = 0.875*sin(phivals(i));
            tmpYcoord = ellipticity*0.875*cos(phivals(i));

            
            if ((i<=3) | (i>=7))
                tmpstr = sprintf('$\\uparrow$');
            else
                tmpstr = sprintf('$\\downarrow$');
            end
            
            tmpcolor = colors.verydarkgrey;

            tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                        'fontsize',24,...
                        'units','data',...
                        'horizontalalignment','center',...
                        'color',tmpcolor, ...
                        'rotation',textangles(i), ...
                        'verticalalignment','middle',...
                        'interpreter','latex');

        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% annotations: rows
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        idim = indexpair(1);
        otherdims = setdiff([1:3],idim);

        N = floor((max(word_ousiometric_avgs(idim,:)) - min(word_ousiometric_avgs(idim,:)))/annotation_settings.binwidth);

        binedges = linspace(min(word_ousiometric_avgs(idim,:)),...
                            max(word_ousiometric_avgs(idim,:)),...
                            N + 1);

        offsets = [0]; %% [-.56 -.28 .28 .56];
        for j=1:length(offsets)
            offset = offsets(j);
            for i=[1:Nmedianannotations, N-Nmedianannotations+1:N]
                if (i<=10)
                    angle = 90;
                else
                    angle = 90;
                end
                indices = find(((word_ousiometric_avgs(idim,:))>=binedges(i)) & ...
                               ((word_ousiometric_avgs(idim,:))<binedges(i+1)));
                [~,index] = min(sum((word_ousiometric_avgs(indexpair,indices)-[offset 0]').^2));
                if (length(index)>0)
                    indices(index);
                    word = zipfwords{indices(index)};
                    
                    tmpXcoord = binedges(i) + annotation_settings.binwidth/2;
                    tmpYcoord = offset;
                    tmpstr = sprintf('%s',word);

                    if (mod(i+j,2)==1)
                        tmpcolor = colors.darkergrey;
                    else
                        tmpcolor = colors.verydarkgrey;
                    end

                    tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                                'fontsize',18,...
                                'units','data',...
                                'horizontalalignment','center',...
                                'color',tmpcolor, ...
                                'rotation',angle,...
                                'verticalalignment','middle',...
                                'interpreter','latex');

                end    
            end    
        end        


        %% change axis line width (default is 0.5)
        %% set(tmpaxes(axesnum),'linewidth',2)

        %% fix up tickmarks
        set(gca,'xtick',[-1:.2:1]);
        %% set(gca,'xticklabel',{'','',''})
        set(gca,'ytick',[-1:.2:1]);
        %% set(gca,'yticklabel',{'','',''})

        %% the following will usually not be printed 
        %% in good copy for papers
        %% (except for legend without labels)

        %% remove a plot from the legend
        %% set(get(get(tmph,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

        %% %% legend

        %% tmplh = legend('stuff',...);
        %% tmplh = legend('','','');
        %% 
        %% tmplh.Interpreter = 'latex';
        %% set(tmplh,'position',get(tmplh,'position')-[x y 0 0])
        %% %% change font
        %% tmplh_obj = findobj(tmplh,'type','text');
        %% set(tmplh_obj,'FontSize',20);
        %% %% remove box:
        %% legend boxoff

        %% use latex interpreter for text, sans serif

        clear tmpstr;

        tmpstr = sprintf('%s $%s$', ...
                         dimensions(dim1,coordsindex).namelc,...
                         dimensions(dim1,coordsindex).notation);



        tmpxlab=xlabel(tmpstr,...
                       'fontsize',24,...
                       'verticalalignment','top',...
                       'interpreter','latex');


        tmpstr = sprintf('%s $%s$', ...
                         dimensions(dim2,coordsindex).namelc,...
                         dimensions(dim2,coordsindex).notation);


        tmpylab=ylabel(tmpstr,...
                       'fontsize',24,...
                       'verticalalignment','bottom',...
                       'interpreter','latex');

        %% set(tmpxlab,'position',get(tmpxlab,'position') - [0 .1 0]);
        %% set(tmpylab,'position',get(tmpylab,'position') - [.1 0 0]);

        %% set 'units' to 'data' for placement based on data points
        %% set 'units' to 'normalized' for relative placement within axes
        %% tmpXcoord = ;
        %% tmpYcoord = ;
        %% tmpstr = sprintf(' ');
        %% or
        %% tmpstr{1} = sprintf(' ');
        %% tmpstr{2} = sprintf(' ');
        %%
        %% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
        %%     'fontsize',24,...
        %%     'units','normalized',...
        %%     'horizontalalignment','left',...
        %%     'verticalalignment','top',...
        %%     'interpreter','latex');

        %% label (A, B, ...)
        
        clear tmpstr;
        tmpstr = sprintf('\\ %s ',char('A'+axesnum-1));
        
        tmplabelXcoord= -0.90;
        tmplabelYcoord= +0.90;
        tmph = text(tmplabelXcoord,tmplabelYcoord,...
                    tmpstr,...
                    'fontsize',16,...
                    'units','data',...
                    'interpreter','latex');
        set(tmph,'horizontalalignment','center');
        set(tmph,'backgroundcolor',colors.lightgrey);
        set(tmph,'edgecolor',[0 0 0]);
        %%        set(tmph,'color',colors.lightergrey);
        set(tmph,'linestyle','-');
        set(tmph,'linewidth',1);
        set(tmph,'margin',1);


        %% tmplabelh = addlabel5(axesnum,0.82,0.95,18);        

        if (mod(axesnum,3)==1)
            tmpstr = sprintf('\\,%s\\,',...
                             coordsfull{coordsindex});

            tmplabelXcoord= -1.6;
            tmplabelYcoord= +0;
            tmph = text(tmplabelXcoord,tmplabelYcoord,...
                        tmpstr,...
                        'fontsize',24,...
                        'units','data',...
                        'rotation',90,...
                        'interpreter','latex');
            set(tmph,'horizontalalignment','center');
            set(tmph,'backgroundcolor',colors.lightgrey);
            set(tmph,'edgecolor',[0 0 0]);
            set(tmph,'linestyle','-');
            set(tmph,'linewidth',1);
            set(tmph,'margin',1);
        end

        

        if (axesnum==1)
            %%            tmpstr = sprintf('$\\Longleftarrow$ SVD
            %%            \\textbf{---}');


            clear tmpstr;
            tmpstr{1} = sprintf('SVD');
            tmpstr{2} = sprintf('$\\big\\downarrow$');

            tmplabelXcoord= -1.50;
            tmplabelYcoord= -1.20;
            tmph = text(tmplabelXcoord,tmplabelYcoord,...
                        tmpstr,...
                        'fontsize',20,...
                        'rotation',0,...
                        'units','data',...
                        'interpreter','latex');
            set(tmph,'horizontalalignment','center');
            
        end 

        if (axesnum==4)
%%             clear tmpstr;
%%             tmpstr{1} = sprintf('$\\leftarrow$ Rotate \\textbf{---}');
%%             tmpstr{2} = sprintf('$G$-$E$ plane +$\\pi/4$',...
%%                                 coordslist{coordsindex});
%% 
%%             tmplabelXcoord= -1.48;
%%             tmplabelYcoord= -1.20;
%%             tmph = text(tmplabelXcoord,tmplabelYcoord,...
%%                         tmpstr,...
%%                         'fontsize',20,...
%%                         'rotation',90,...
%%                         'units','data',...
%%                         'interpreter','latex');
%%             set(tmph,'horizontalalignment','center');
            
            clear tmpstr;
            tmpstr{1} = sprintf('Rotate');
            tmpstr{2} = sprintf('$G$-$E$ plane');
            tmpstr{3} = sprintf('by $\\pi/4$');
            tmpstr{4} = sprintf('$\\big\\downarrow$');
            
            tmplabelXcoord= -1.50;
            tmplabelYcoord= -1.20;
            tmph = text(tmplabelXcoord,tmplabelYcoord,...
                        tmpstr,...
                        'fontsize',20,...
                        'rotation',0,...
                        'units','data',...
                        'interpreter','latex');
            set(tmph,'horizontalalignment','center');
            
        end 

        
        tmpstr = sprintf('\\,$%s$ vs.\\ $%s$\\,',...
                         dimensions(dim2,coordsindex).notation, ...
                         dimensions(dim1,coordsindex).notation);


        tmpXcoord = +1.03;
        tmpYcoord = +0.95;
        tmpcolor = colors.verydarkgrey;
        tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
                    'fontsize',20,...
                    'units','data',...
                    'horizontalalignment','right',...
                    'verticalalignment','bottom',...
                    'color',tmpcolor, ...
                    'rotation',0, ...
                    'interpreter','latex');
        set(tmph,'backgroundcolor',colors.lightgrey);
        set(tmph,'edgecolor',[0 0 0]);
        set(tmph,'linestyle','-');
        set(tmph,'linewidth',1);
        set(tmph,'margin',1);
        
        
        %% rarely used (text command is better)
        %% title(' ','fontsize',24,'interpreter','latex')
        %% 'horizontalalignment','left');
        %% tmpxl = xlabel('','fontsize',24,'verticalalignment','top');
        %% set(tmpxl,'position',get(tmpxl,'position') - [ 0 .1 0]);
        %% tmpyl = ylabel('','fontsize',24,'verticalalignment','bottom');
        %% set(tmpyl,'position',get(tmpyl,'position') - [ 0.1 0 0]);
        %% title('','fontsize',24)



        %% %% tmph = plot(0,0,'ko');
        %% %% set(tmph,'markerfacecolor',colors.darkgrey);
        %% %% set(tmph,'markeredgecolor',colors.verydarkgrey);
        %% 
        %% clear tmpstr;
        %% tmpstr{1} = sprintf('$%s \\sim %4.2f %s$', ...
        %%                     dimensions_old(3).notation,...
        %%                     slope,...
        %%                     dimensions_old(2).notation);
        %% tmpstr{2} = sprintf('$r \\simeq %4.2f$', ...
        %%                     corrsVAD(2,3));
        %% 
        %% tmpXcoord = 0.20;
        %% tmpYcoord = 0;
        %% tmph = text(tmpXcoord,tmpYcoord,tmpstr,...
        %%             'fontsize',20,...
        %%             'units','data',...
        %%             'horizontalalignment','center',...
        %%             'color',tmpcolor, ...
        %%             'rotation',0, ...
        %%             'verticalalignment','middle',...
        %%             'interpreter','latex');

        
        
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% automatic creation of image file (pdf, postscript, png, ...)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (isfield(settings,'imageformat'))
    if (isfield(settings.imageformat,'open'))
        imageformat.open = settings.imageformat.open;
    else
        imageformat.open = 'yes';
    end
else
    imageformat.open = 'yes';
end

%% for pdf, without name/date
imageformat.type = 'pdf';
imageformat.dpi = 600;
imageformat.deleteps = 'yes';
%% imageformat.open = 'yes';
imageformat.copylink = 'no';

print_universal(tmpfilenoname,imageformat);

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %% png with name/date
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 
%% axesnum = 1;
%% axes(tmpaxes(axesnum));
%% %% tmpaxes(axesnum) = axes('position',axes_positions(axesnum).box);
%% 
%% clear tmpstr;
%% tmpstr = sprintf(' otcompstorylab');
%% %% dates covered: %s to %s', ... 
%% %%                 datestr(startdatenum,'yyyy/mm/dd'), ...
%% %%                 datestr(datenum2,'yyyy/mm/dd'));
%% 
%% tmpXcoord = 0.00;
%% tmpYcoord = 0.00;
%% 
%% text(tmpXcoord,tmpYcoord,tmpstr,...
%%      'fontsize',16,...
%%      'units','normalized',...
%%      'horizontalalignment','left',...
%%      'verticalalignment','bottom',...
%%      'rotation',0,...
%%      'color',0.7*[1 1 1],...
%%      'interpreter','latex')
%% 
%% print_universal(tmpfiletwittername,imageformat);
%% 
%% tmpcommand = sprintf('convert -background white -flatten -geometry 1600x -density 600 %s.pdf %s.png', ...
%%                      tmpfilesharename, ...
%%                      tmpfilesharename);
%% system(tmpcommand);
%% 
%% tmpcommand = sprintf('permanentshare-uvm.pl %s.png', ...
%%                      tmpfiletwittername);
%% system(tmpcommand);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% archivify (0 = off, non-zero = on)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

archiveswitch = 0;
figarchivify(tmpfilenoname,archiveswitch);


%% prevent hidden figure clutter/bloat
%% may need to switch this off for some test
close(tmpfigh);

%% clean up tmp* files
clear tmp*

more on;
