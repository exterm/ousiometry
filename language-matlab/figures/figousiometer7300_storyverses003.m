more off;

%% make a figure for each fictional work's characters

clear general_settings;

load ../analysis/character_data.mat;

process_storyverse_names;


%% some of what we should have:
%% 
%% A: Ntraits x Ncharacters matrix
%%    Mid score of 50 removed uniformly
%%    Rescaled so all values fall in -1/2 to 1/2
%% 
%% characters_table
%% e.g.,
%% ID     FictionalWork    CharacterDisplayName 
%% {'GP/1'}    {'The Good Place'}    {'Eleanor Shellstrop'}
%% 
%% bap_table: Bipolar adjective pairs for semantic differentials
%% e.g.,
%% ID    low_leftAnchor  high_rightAnchor     
%% {'BAP203'}    {'cool'}    {'dorky'}
%% 
%% SVD matrices: U, Sigma, V, 
%% sigma values: sigmas
%% 
%% >> whos
%% 
%%  Name                    Size               Bytes  Class     Attributes
%% 
%%   Sigma                 235x800            1504000  double              
%%   U                     235x235             441800  double              
%%   V                     800x800            5120000  double              
%%   bap_table             235x3                85437  table               
%%   characters_table      800x3               301319  table               
%%   sigmas                235x1                 1880  double           
%% 

Ntypes(1) = size(A,1);
Ntypes(2) = size(A,2);

typenames1 = bap_typenames;
typenames2 = character_typenames;

%% U or V
%% e.g., if 1 = traits, 2 = characters
%% then
%% if maindim = 1, eigentraits will be described by characters
%% if maindim = 2, eigencharacters will be described by traits

%% eigentraits;
%% primary_dim = 1;
%% eigencharacters:
primary_dim = 2;

second_dim = 3 - primary_dim;

%% two dimensions for ousiogram, from U or V
%% choice of eigentraits or eigencharacters
dim1 = 3;
dim2 = 2;



%% lexicon style plot, no popularity
token_frequencies = ones(Ntypes(primary_dim),1);

%% 
if (primary_dim == 1)
    type_dimension_weights = A * V(:,[dim1 dim2]);
    coordnames{1} = sprintf('\\hat{u}_{%d}',dim1);
    coordnames{2} = sprintf('\\hat{u}_{%d}',dim2);
    typenames = typenames1;
else
    type_dimension_weights = transpose(U(:,[dim1 dim2])' * A);
    %%    coordnames{1} = sprintf('\\hat{v}_{%d}',dim1);
    %%    coordnames{2} = sprintf('\\hat{v}_{%d}',dim2);
    coordnames{1} = sprintf('S',dim1);
    coordnames{2} = sprintf('D',dim2);
    typenames = typenames2;
end


i=0;
i=i+1;
dimension_settings(i).name = 'Structure';
dimension_settings(i).namelc = 'structure';
dimension_settings(i).notation = coordnames{1};
dimension_settings(i).range = [-1 1];
dimension_settings(i).positive = 'less rigid';
dimension_settings(i).negative = 'more rigid';
dimension_settings(i).positive1word = 'unstructued';
dimension_settings(i).negative1word = 'structured';
i=i+1;
dimension_settings(i).name = 'Danger';
dimension_settings(i).namelc = 'danger';
dimension_settings(i).notation = coordnames{2};
dimension_settings(i).range = [-1 1];
dimension_settings(i).positive = 'more of a sinner';
dimension_settings(i).negative = 'more of a saint';
dimension_settings(i).positive1word = 'dangerous';
dimension_settings(i).negative1word = 'safe';




%% coordinate system

general_settings.colorbarpos = [-.975 +0.60];


general_settings.coordnames = coordnames;


general_settings.showellipse = 'no';

general_settings.overallmaxtoken_frequencies = 600;
general_settings.colorbartoken_frequencies = [1, 25:25:250];
%% general_settings.colormaptype = 'normalized';


%% no underlying annotations
%% general_settings.do_not_annotate = words;




%% 
theta = -45*pi/180;

%% rotate by pi/4
Rot2d = [
      cos(theta) -sin(theta);
      sin(theta) cos(theta);
    ];

%% general_settings.do_annotate.altvalues = [pleasurescores, arousalscores] * Rot2d';


i=0;
i=i+1;
general_settings.do_annotate.categories{i} = 'dangerous';
i=i+1;
general_settings.do_annotate.categories{i} = 'dangerous-unpredictable';
i=i+1;
general_settings.do_annotate.categories{i} = 'unpredictable';
i=i+1;
general_settings.do_annotate.categories{i} = 'safe-unpredictable';
i=i+1;
general_settings.do_annotate.categories{i} = 'safe';
i=i+1;
general_settings.do_annotate.categories{i} = 'safe-rigid';
i=i+1;
general_settings.do_annotate.categories{i} = 'rigid';
i=i+1;
general_settings.do_annotate.categories{i} = 'dangerous-rigid';




%% general_settings.horizontal_axis_label = sprintf('%s', ...
%%                 'displeasure-pleasure');

%% general_settings.vertical_axis_label = sprintf('%s', ...
%%                 'sleep-arousal');


general_settings.annotate_internally = 'no';
%% general_settings.phivals = [0:45:315]*pi/180;

general_settings.Nannotations_horizontal = 5;
general_settings.Nannotations_vertical = 5;

general_settings.imageformat.open = 'no';

%% set token_frequencies to ones for lexicon
%% set token_frequencies to zipf distribution frequencies for a
%% corpus

%% no underlying annotations
general_settings.do_not_annotate = col(character_typenames);

for storyverse_i=1:length(storyverses) 
    storyverse = storyverses{storyverse_i};
    fprintf(1,'\n%03d: %s (%s)...\n',...
            storyverse_i,...
            storyverse,...
            storyverse_abbrev{storyverse_i});
    
    indices = find(strcmp(storyverse,storyverses_full_list));
    %% overlay annotations
    general_settings.do_annotate.types = character_typenames(indices);
    general_settings.do_annotate.kind = '';
    general_settings.do_annotate.offset_angles = 0;
    for i=1:length(general_settings.do_annotate.types)
        general_settings.do_annotate.kind{i,1} = 'polar';
        general_settings.do_annotate.offset_angles(i,1) = 0;
    end
    
    
    general_settings.title = sprintf('%s-%s space for %s',...
                                     dimension_settings(1).name, ...
                                     dimension_settings(2).name, ...
                                     storyverse);

    general_settings.filetag = sprintf('storyverses_%s_%03d_003_%s', ...
                                       storyverses_full_list_for_file_names{storyverse_i},...
                                       storyverse_i,...
                                       storyverse_abbrev{storyverse_i});
    
    things = figousiometer7300(typenames, ...
                               token_frequencies, ...
                               type_dimension_weights, ...
                               dimension_settings, ...
                               general_settings);

    maxcolorindices = things.maxcolorindex_data;

end

more on;

%% !combinepdfs figousiometer7300/figousiometer7300_storyverses003_* figousiometer7300_storyverses003_combined.pdf
