Suggested set up on Mac/Unix/Linux machines:
# Matlab setup:
   1. Script directory location:
   ~/matlab/ousiometry

   1. Add following snippet to ~/matlab/startup.m

   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   %% add all subdirectories in ~/matlab
   paths_to_add = genpath('~/matlab');
   %% remove .git, archive subdirectories
   paths_to_add = regexprep(paths_to_add,'~\/matlab[^:]*?\/\.git.*?:','','all');
   paths_to_add = regexprep(paths_to_add,'~\/matlab\/archive.*?:','','all');

   %% add paths
   addpath(paths_to_add);

   %% current directory
   addpath .

   +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

   Explanation for above:
   - ~/matlab is placed Matlab's path by default
   - genpath will find subdirectories in ~/matlab to add to path
   - remove ones we don't need with regexprep (.git and archive; adjust as needed)
   - add to paths with addpath(paths_to_add)

   1. the command print_universal constructs pdfs and opens them
      - ideally, matlab figure viewer should never be used
      - pdf is a final product
      - option for png is available; requires imagemagic (see script for instructions)
      - print_universal logs figure making activity (see script for details)

# Python setup:
`git clone git@gitlab.com:petersheridandodds/ousiometry.git`

`cd ousiometry`

(you should probably use a venv when developing out this codebase!)

```
python3 -m venv venv
. venv/bin/activate
```

`pip3 install -e .` to install as an editable package.